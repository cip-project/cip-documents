CIP Documents
=============

This repository is where keeps all documents at one place for all
working groups of the CIP projects to meet secure development process
definced at IEC 62443-4-1 which require to maintain documents and their
versions.

Management policy
-----------------

This repository will be maintained by a few security members to meet
secure development process, thus branches in this repository will be
protected by restricting members enabling to push and merge.

License
-------

The license of all documentation in this repository follows the
intellectual property policy in the CIP Charter. See section 14-e in
`the CIP Charter <https://www.cip-project.org/about/charter>`__.

Guide
-----

This section will give brief descriptions about each document to make
navigating this repository easier. Non-document files will not be
explained here. - cip-project - cip-documents - developer - event -
process - security - testing - user

Developer
~~~~~~~~~
+---------------------------------------------------+--------------------------------+
| Name                                              | Description                    |
+===================================================+================================+
| `FOSS_Security_Study_Summary <https://gitlab.com/ | Presentation on security       |
| cip-project/cip-documents/-/blob/                 | increases in Debian over time. |
| master/developer/investigation/FO                 |                                |
| SS_Security_Study_Summary.pdf>`__                 |                                |
+---------------------------------------------------+--------------------------------+

Event
~~~~~

+--------------------------------------------+----------------------------------+
| Name                                       | Description                      |
+============================================+==================================+
| `Introduction of CIP Software              | Presentation CIP Software Update |
| Updates Working                            | WG.                              |
| Group <http                                |                                  |
| s://gitlab.com/cip-project/cip-do          |                                  |
| cuments/-/blob/master/event/2019/          |                                  |
| sw_updates_wg_mini-summit.pdf>`__          |                                  |
+--------------------------------------------+----------------------------------+
| `CIP Security towards achieving            | Presentation CIP Security WG.    |
| industrial grade                           |                                  |
| security <http                             |                                  |
| s://gitlab.com/cip-project/cip-do          |                                  |
| cuments/-/blob/master/event/2020/          |                                  |
| CIP_Security_WG_OSSNA_r02.pdf>`__          |                                  |
+--------------------------------------------+----------------------------------+
| `Threat modelling - Key                    | Presentation of CIP Security WG  |
| methodologies and applications             | on Threat modeling in CIP.       |
| from OSS CIP(CIP)                          |                                  |
| perspective <https://gitlab.com/cip-projec |                                  |
| t/cip-documents/-/blob/master/eve          |                                  |
| nt/2020/%5BELCE%5D%20Threat%20mod          |                                  |
| elling%20-%20Key%20methodologies%          |                                  |
| 20and%20applications%20from%20OSS          |                                  |
| %20CIP(CIP)%20perspective.pdf>`__          |                                  |
+--------------------------------------------+----------------------------------+


Process
~~~~~~~

+-----------------------------------+-----------------------------------+
| Name                              | Description                       |
+===================================+===================================+
| `CIP File                         | The primary objective of this     |
| Integrity <https://gitlab.com/cip | document is to explain about how  |
| -project/cip-documents/-/blob/mas | file integrity for CIP            |
| ter/process/file_integrity.rst>`__| deliverables is achieved.         |
+-----------------------------------+-----------------------------------+
| `CIP Roles and Responsibility     | The primary objective of this     |
| Matrix <https://gitl              | document is to show the roles in  |
| ab.com/cip-project/cip-documents/ | CIP with their responsibilities   |
| -/blob/master/process/raci.rst>`__| and accountabilities. It is also  |
|                                   | shwon which roles should be       |
|                                   | consulted and/or informed for     |
|                                   | certain actions and which         |
|                                   | qualifications, if any, are       |
|                                   | needed to fulfill a role.         |
+-----------------------------------+-----------------------------------+
| `CIP Secure Development           | This document is based on         |
| Process <h                        | IEC-62443-4-1 (Edition 1.0        |
| ttps://gitlab.com/cip-project/cip | 2018-01) secure development       |
| -documents/-/blob/master/process/ | process requirements.The          |
| secure_development_process.rst>`__| Objective is to adhere            |
|                                   | IEC-62443-4-1 secure development  |
|                                   | process requirements in CIP       |
|                                   | development as much as possible.  |
+-----------------------------------+-----------------------------------+

Security
~~~~~~~~

+-----------------------------------------------------------------------------------------------+-----------------------------------+
| Name                                                                                          | Description                       |
+===============================================================================================+===================================+
| `CIP Security Coding GuideLines <https                                                        | This document explains how CIP    |
| ://gitlab.com/cip-project/cip-doc                                                             | Project and its upstream projects |
| uments/-/blob/master/security/CIP                                                             | are following security coding     |
| -Security-CodingGuideLines.rst>`__                                                            | guidelines.                       |
+-----------------------------------------------------------------------------------------------+-----------------------------------+
| `Static analysis tools for CIP                                                                | This document explains how CIP    |
| packages <https://                                                                            | Project executes SCA with some    |
| gitlab.com/cip-project/cip-docume                                                             | explanation on how to use some    |
| nts/-/blob/master/security/CIP-Se                                                             | SCA software.                     |
| curity-StaticAnalysisTools.rst>`__                                                            |                                   |
+-----------------------------------------------------------------------------------------------+-----------------------------------+
| `CIP Development Environment                                                                  | The primary objective of this     |
| Security <https://                                                                            | document is to document current   |
| gitlab.com/cip-project/cip-docume                                                             | development environment security, |
| nts/-/blob/master/security/develo                                                             | development flow and how security |
| pment_environment_security.rst>`__                                                            | is maintained.                    |
+-----------------------------------------------------------------------------------------------+-----------------------------------+
| `IEC 62443-4-2 App & HW                                                                       | The primary objective of this     |
| Guidelines <ht                                                                                | document is to provide guidelines |
| tps://gitlab.com/cip-project/cip-                                                             | to CIP users for meeting          |
| documents/-/blob/master/security/                                                             | IEC-62443-4-2 security            |
| iec62443-app-hw-guidelines.rst>`__                                                            | requirements. The document        |
|                                                                                               | explains about each IEC-62443-4-2 |
|                                                                                               | requirements whether it has       |
|                                                                                               | already been met by CIP. In       |
|                                                                                               | addition this document also       |
|                                                                                               | explains about iec security layer |
|                                                                                               | added in CIP to meet              |
|                                                                                               | IEC-62443-4-2 security            |
|                                                                                               | requirements.                     |
+-----------------------------------------------------------------------------------------------+-----------------------------------+
| `User Security                                                                                | This document contains items      |
| Manual <https://gitlab.com/cip-projec                                                         | identified during IEC-62443-4-1   |
| t/cip-documents/-/blob/master/sec                                                             | and IEC-62443-4-2 Gap Assessment  |
| urity/user_security_manual.rst>`__                                                            | for user security manual.         |
+-----------------------------------------------------------------------------------------------+-----------------------------------+
| `OWASP Top 10 Vulnerabilities                                                                 | The primary objective of this     |
| Monitoring <https://gitlab                                                                    | document is to explain about how  |
| .com/cip-project/cip-documents/-/                                                             | various OWASP. top 10             |
| blob/master/security/owasp_top10_                                                             | vulnerabilities are handled in    |
| vulnerabilities_monitoring.rst>`__                                                            | CIP.                              |
+-----------------------------------------------------------------------------------------------+-----------------------------------+
| `CIP Private Key Management <https://gitlab.com/cip-project/cip-documents/-/blob/master/secur | The primary objective of this     |
| ity/private_key_management.rst>`__                                                            | document is to explain about how  |
|                                                                                               | various private keys used in CIP  |
|                                                                                               | development are maintained and    |
|                                                                                               | kept secure and confidential.     |
+-----------------------------------------------------------------------------------------------+-----------------------------------+
| `CIP Security                                                                                 | This document is intended to      |
| Requirements <https://gitlab.com/cip-project/cip-documents/-/blob/master/secu                 | capture CIP security requirements |
| rity/security_requirements.rst>`__                                                            | based on IEC-62443-4-2 standard.  |
+-----------------------------------------------------------------------------------------------+-----------------------------------+
| `CIP Threat                                                                                   | The primary objective of this     |
| Modeling <https://gitlab.com/cip-pr                                                           | document is to create Threat      |
| oject/cip-documents/-/blob/master                                                             | Model for CIP reference platform. |
| /security/threat_modelling.rst>`__                                                            |                                   |
+-----------------------------------------------------------------------------------------------+-----------------------------------+


Testing
~~~~~~~

+-----------------------------------------+-----------------------------------+
| Name                                    | Description                       |
+=========================================+===================================+
| `CIP_IEC-62443-4-2                      | Overview of the CIP 62443-4-2     |
| _Security_TestCases <https://gitl       | test cases.                       |
| ab.com/cip-project/cip-documents/       |                                   |
| -/blob/master/testing/core/iec-62       |                                   |
| 443-evaluation/CIP_IEC-62443-4-2_       |                                   |
| Security_TestCases_Rev03.xlsx>`__       |                                   |
+-----------------------------------------+-----------------------------------+
| `CIP Penetration                        | The primary objective of this     |
| Testing <https://gitlab.com/cip-project | document is to identify suitable  |
| /cip-documents/-/blob/master/test       | penetration testing tool and      |
| ing/CIP-PenetrationTesting.rst>`__      | document the process how this can |
|                                         | be re-used by CIP end users for   |
|                                         | their specific use cases.         |
+-----------------------------------------+-----------------------------------+

User
~~~~

+--------------------------------------+--------------------------+
| Name                                 | Description              |
+======================================+==========================+
| `CIP User                            | This document is a user  |
| Manual <https://gitlab.com/cip-proje | perspective overview and |
| ct/cip-documents/-/blob/master/us    | technical guide for CIP. |
| er/user_manual/user_manual.rst>`__   |                          |
+--------------------------------------+--------------------------+

