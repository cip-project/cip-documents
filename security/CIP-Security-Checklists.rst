Checklist for compliance to IEC-62443-4-1
=========================================

Purpose
-------

This document contains compliance checklists to evaluate adherence to
IEC-62443-4-1. This document was created by the Security Working group
of the CIP-project.

Scope
-----

This document shall be used by the CIP-project to document adherence to
IEC-62443-4-1:2018 for every release.

Checklists
----------

Organization & Management
~~~~~~~~~~~~~~~~~~~~~~~~~

Roles and Responsibilities
^^^^^^^^^^^^^^^^^^^^^^^^^^

For each role, personnel shall be trained to have adequate security
experience and training to fullfil their role and obligations. This
shall be ensured by managing the competencies of staff and providing
training and education to all personnel according to their role.

-  [ ] Security relevant roles are identified.
-  [ ] Appropriate experience is available or training planned.

Supply Chain
------------

-  [ ] Security processes and requirements are applied to any third-party
   components.
-  [ ] Issues of third-party components are managed.

Development Process
-------------------

System and Software Development
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  [ ] The product security context is well-defined.
-  [ ] A threat model has been created and is continuosly updated.
-  [ ] Security requirements have been defined.
-  [ ] Secure design best practices have been followed, including review.
-  [ ] Secure implementation best practices have been followed, including
   review.
-  [ ] Secure coding rules have been defined and followed.
-  [ ] Security testing has been planned, executed and findings have been
   followed to closure.

Vulnerability Management
------------------------

-  [ ] The product is tested for known vulnerabilities.
-  [ ] Known vulnerabilties are managed.
-  [ ] Mitigations or compensating countermeasure are defined and document
   for all known vulnerabilities.
-  [ ] Sources for known vulnerabilities are constantly monitored.
-  [ ] Incidents and vulnerabilities are actively managed.
-  [ ] Security patches are provided securely and in a timely manner.
-  [ ] Security patches are tested to check: Does the patch fix the
   vulnerability? Does the patch have side-effects? What is the risk of
   applying or not applying?

Documentation
-------------

Release documentation
~~~~~~~~~~~~~~~~~~~~~

-  [ ] The release documentation contains the name of the product, the
   version and unambiguous and unique identifiers to ensure that the
   delivered product is complete and correct.
-  [ ] The release documentation contains assumptions about the expected
   operational environment.
-  [ ] The release documentation contains information about known and
   fixed vulnerabilities.
-  [ ] The release documentation contain instructions about mitigations
   and compensating countermeasures (if applicable).

User documentation
~~~~~~~~~~~~~~~~~~

-  [ ] The product documentation contains information about its
   defense-in-depth strategy.
-  [ ] The user documentation contains instructions how to operate the
   product securely. In particular with respect to handling of user
   accounts and permissions.
-  [ ] The user documentation contains information about how to harden the
   product.
-  [ ] The user documentation contains information about the secure
   disposal.
