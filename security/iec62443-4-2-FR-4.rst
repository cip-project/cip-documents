iec62443-4-2-FR-4
=================

.. contents::

+--------+---------------------------+------------------+---------------------------+------------------+----------------------------+
| Req ID | Requirement name          | Supported        | Need                      | Need HW          | Status if                  | 
|        |                           | by CIP           | ap                        | solution         | supported                  | 
|        |                           |                  | plication                 |                  | by CIP                     | 
|        |                           |                  | support                   |                  |                            |
+========+===========================+==================+===========================+==================+============================+
| CR-4.1 | Information               | TRUE             | TRUE                      | FALSE            | In-progress                |
|        | confidentiality - at rest |                  |                           |                  | Investigation to add       |
|        |                           |                  |                           |                  | packages                   |
|        |                           |                  |                           |                  | in progress                |
+--------+---------------------------+------------------+---------------------------+------------------+----------------------------+
| CR-4.2 | Information               | FALSE            | TRUE                      | FALSE            | N.A.                       |
|        | persistence               |                  |                           |                  |                            |
+--------+---------------------------+------------------+---------------------------+------------------+----------------------------+
| CR-4.2 | Erase of                  | TRUE             | TRUE                      | FALSE            | CompletedAdded             |
| RE(1)  | shared                    |                  |                           |                  | package                    |
|        | memory                    |                  |                           |                  | ipcrm                      |
|        | resources                 |                  |                           |                  |                            |
+--------+---------------------------+------------------+---------------------------+------------------+----------------------------+
| CR-4.2 | Erase                     | TRUE             | TRUE                      | FALSE            | CompletedAdded             |
| RE(2)  | verification              |                  |                           |                  | package                    |
|        |                           |                  |                           |                  | ipcrm                      |
+--------+---------------------------+------------------+---------------------------+------------------+----------------------------+
| CR-4.3 | Use of                    | TRUE             | FALSE                     | FALSE            | Completed                  |
|        | cryptography              |                  |                           |                  | added                      |
|        |                           |                  |                           |                  | openssl                    |
|        |                           |                  |                           |                  | package                    |
+--------+---------------------------+------------------+---------------------------+------------------+----------------------------+

Tests reference and CIP recommendation
--------------------------------------

+--------------+----------------------------+-------------------------------+----------------------+
| Req ID       | Status if                  | IEC-62443-4-2                 | CIP                  | 
|              | supported by               | tests reference               | recommendation       | 
|              | CIP                        |                               |                      |
+==============+============================+===============================+======================+
| CR-4.1       | In-progress                | None                          | CIP platform         |
|              | Investigation              |                               | complies to          |
|              | to add packages            |                               | this requirement CIP |
|              | in progress                |                               | users need to        |
|              |                            |                               | utilize the          |
|              |                            |                               | capabilities         |
|              |                            |                               | provided by CIP      |
|              |                            |                               | and encrypt          |
|              |                            |                               | data at rest         |
+--------------+----------------------------+-------------------------------+----------------------+
| CR-4.2       | N.A.                       | None                          | CIP platform         |
|              |                            |                               | does not             |
|              |                            |                               | support this         |
|              |                            |                               | requirementCIP       |
|              |                            |                               | member needs to      |
|              |                            |                               | provide              |
|              |                            |                               | application          |
|              |                            |                               | which can            |
|              |                            |                               | achieve              |
|              |                            |                               | information          |
|              |                            |                               | persistance,         |
|              |                            |                               | CIP provides         |
|              |                            |                               | underneath           |
|              |                            |                               | packages to          |
|              |                            |                               | meet this            |
|              |                            |                               | requirement          |
+--------------+----------------------------+-------------------------------+----------------------+
| CR-4.2 RE(1) | Completed Added            | `TC_CR4.2-RE1_1               | CIP meets this       |
|              | package ipcrm              | <https://gitlab               | requirement by       |
|              |                            | .com/cip-projec               | adding the           |
|              |                            | t/cip-testing/c               | packages.CIP         |
|              |                            | ip-security-tes               | users should         |
|              |                            | ts/-/tree/maste               | use CIP              |
|              |                            | r/iec-security-               | capabilities         |
|              |                            | tests/singlenod               | and make sure        |
|              |                            | e-testcases/TC_               | volatile memory      |
|              |                            | CR4.2-RE1_1>`__               | contents are         |
|              |                            |                               | deleted as well      |
|              |                            |                               | as any data          |
|              |                            |                               | buffer               |
|              |                            |                               | allocated by         |
|              |                            |                               | application is       |
|              |                            |                               | reset before         |
|              |                            |                               | deleting or          |
|              |                            |                               | allocating to        |
|              |                            |                               | other                |
|              |                            |                               | processes.           |
+--------------+----------------------------+-------------------------------+----------------------+
| CR-4.2 RE(2) | Completed Added            | `TC_CR4.2-RE2_1               | CIP users            |
|              | package ipcrm              | <https://gitlab               | should support       |
|              |                            | .com/cip-projec               | to confirm and       |
|              |                            | t/cip-testing/c               | acknowledge the      |
|              |                            | ip-security-tes               | erased contents      |
|              |                            | ts/-/tree/maste               | are not              |
|              |                            | r/iec-security-               | accessible.          |
|              |                            | tests/singlenod               |                      |
|              |                            | e-testcases/TC_               |                      |
|              |                            | CR4.2-RE2_1>`__               |                      |
+--------------+----------------------------+-------------------------------+----------------------+
| CR-4.3       | Completed added            | Refer openssl                 | Refer details        |
|              | openssl package            | tests for                     | of this              |
|              |                            | CR-1.9                        | requirement in       |
|              |                            |                               | Use of               |
|              |                            |                               | Cryptography         |
|              |                            |                               | document             |
|              |                            |                               | `use_of_cryp         |
|              |                            |                               | tography-cr-4.3      |
|              |                            |                               | .rst <https://gi     |
|              |                            |                               | tlab.com/cip-pr      |
|              |                            |                               | oject/cip-docum      |
|              |                            |                               | ents/-/blob/mas      |
|              |                            |                               | ter/process/use      |
|              |                            |                               | _of_cryptograph      |
|              |                            |                               | y-cr-4.3.rst>`__     |
+--------------+----------------------------+-------------------------------+----------------------+


Additional notes
----------------

Regarding CR-4.2 RE(1)
~~~~~~~~~~~~~~~~~~~~~~

For in-memory applications, APIs can be used, e.g.

-  from OpenSSL ``OPENSSL_cleanse`` This function does not return any
   success status and is guaranteed to work.

If users are required to store sensitive data in files, the following
approaches shall be chosen if possible:

-  Anonymous files ( ``memfd_create`` ) shall be used so that a file
   with secret content will never exist os disk. The deletion is
   guaranteed by the kernel.
-  Secrets shall only be stored in a specially mounted ``ramfs`` so that
   secrets will never be written to persistent memory. (``tmpfs`` might
   be written to swap, so it is not desired here.).

If storing sensitive data on persistent storage is inevitable, ``shred``
shall be used for deletion. Note that this tool is only appropriate for
(non-SMR) hard disks but not for managed flash. No sure approach for SSD
and SD cards exists, unmanaged flash could allow it, but no software
solution currently exists. ``shred`` gives a return value showing if the
operation was successful or not.

An extended file system flag ``FS_SECRM_FL`` exists which should mark
files as to be deleted in a secure manner, but it is nowhere implemented
in the kernel. Implementing it for ramfs and ubifs could be beneficial.

For the meanwhile, if sensitive data is written to persistent memory, it
shall only be written in an encrypted manner so that deleting the key
would be sufficient to render the data unreadable. Note that the key
itself must be stored on a volume that can be deleted securely.

Memory provided provided by the kernel is always zero-initialized
(unless especially enabled by e.g. ``CONFIG_MMAP_ALLOW_UNINITIALIZED``)
and thus, is no concern of information leakage from one program to
another.


