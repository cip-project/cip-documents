CIP Security Requirements
=========================

.. contents::

.. list-table:: Revision History
   :header-rows: 1
                              
   * - Revision No 
     - Date
     - Change description 
     - Author  
     - Reviewed by

   * - 001
     - 2021-02-24
     - Draft security requirement based on IEC-62443-4-x created 
     - Dinesh Kumar
     - TBD

   * - 002
     - 2023-07-10 
     - Updated requirement IDs  
     - Dinesh Kumar
     - TBR

Overview 
--------

This document is intended to capture CIP security requirements based on
IEC-62443-4-2 standard. In future this document should be revised based
on additional security requirements.

IEC-62443-4-2 Requirements 
--------------------------

Following table outlines CIP security requirements. These requirements
are derived from IEC-62443-4-2 security requirements which could be
applicable to CIP.

Each requirement has been assigned one unique identifier as requirement ID 
e.g. CIP_SEC_IEC_FUNC_REQ_1 which can be interpreted as <CIP WG>_<source of req>_<Functional or non-functional>_<Identifier>

+------------------------------+-----------------------------------+-------------------------+-----------------------+
| Requirement                  | Requirement                       | Description             | Source of             | 
| ID                           |                                   |                         | requirement           |
+==============================+===================================+=========================+=======================+
| #REQ-CIP-SEC-IEC-FUNC-REQ_1  | Support human user identification | Following are the key   | IEC-62443-4-2         |
|                              | & authentication,                 | requirements1. System   | CR1.1,                |
|                              | user account                      | should support          | CR1.1(1),             |
|                              | management,                       | identification and      | CR1.3,                |
|                              | password                          | authentication for all  | CR1.5,                |
|                              | management                        | users at all accessible | CR1.7(1),             |
|                              |                                   | interfaces, either      | CR1.7(2)              |
|                              |                                   | locally or by           |                       |
|                              |                                   | integration into a      |                       |
|                              |                                   | system.2. System should |                       |
|                              |                                   | be able to uniquely     |                       |
|                              |                                   | identify and            |                       |
|                              |                                   | authenticate all human  |                       |
|                              |                                   | users3. System should   |                       |
|                              |                                   | support account         |                       |
|                              |                                   | management functions in |                       |
|                              |                                   | order to make changes   |                       |
|                              |                                   | such as activate,       |                       |
|                              |                                   | modify, disable and     |                       |
|                              |                                   | remove user accounts4.  |                       |
|                              |                                   | System should support   |                       |
|                              |                                   | default authenticator   |                       |
|                              |                                   | change5. System should  |                       |
|                              |                                   | support changing        |                       |
|                              |                                   | password once expired   |                       |
+------------------------------+-----------------------------------+-------------------------+-----------------------+
| #REQ-CIP-SEC-IEC-FUNC-REQ_2  | Limit number                      | 1. System should have   | IEC-62443-4-2         |
|                              | of unsuccessful                   | provision to limit      | CR1.11,               |
|                              | login attempts,                   | number of unsuccessful  | CR2.7                 |
|                              | limit on concurrent               | login attempts and once |                       |
|                              | sessions                          | unsuccessful login      |                       |
|                              |                                   | attempts exceed, the    |                       |
|                              |                                   | account should be       |                       |
|                              |                                   | locked either           |                       |
|                              |                                   | temporarily for some    |                       |
|                              |                                   | time or permanently and |                       |
|                              |                                   | admin user should be    |                       |
|                              |                                   | able to unlock it.2.    |                       |
|                              |                                   | System should limit     |                       |
|                              |                                   | number of concurrent    |                       |
|                              |                                   | sessions from any       |                       |
|                              |                                   | user(human, process or  |                       |
|                              |                                   | device) to prevent DoS  |                       |
|                              |                                   | attack                  |                       |
+------------------------------+-----------------------------------+-------------------------+-----------------------+
| #REQ-CIP-SEC-IEC-FUNC-REQ_3  | Multifactor                       | System should provide   | IEC-62443-4-2         |
|                              | authentication                    | multifactor             | CR1.1(2)              |
|                              | for all interfaces                | authentication for      |                       |
|                              | for human users                   | human users on all      |                       |
|                              |                                   | interfaces              |                       |
+------------------------------+-----------------------------------+-------------------------+-----------------------+
| #REQ-CIP-SEC-IEC-FUNC-REQ_4  | Unique                            | System should provide   | IEC-62443-4-2         |
|                              | identifiers                       | capability to integrate | CR1.5(1),             |
|                              | for                               | into a system that      | CR1.9(1),             |
|                              | identifying                       | supports function of    | CR1.14(1)             |
|                              | each account                      | generating unique       |                       |
|                              |                                   | identifiers             |                       |
+------------------------------+-----------------------------------+-------------------------+-----------------------+
| #REQ-CIP-SEC-IEC-FUNC-REQ_5  | Capability                        | System should provide   | IEC-62443-4-2         |
|                              | to access                         | capability to access    | CR1.5(1),             |
|                              | secure                            | secure hardware or TPM  | CR1.9(1),             |
|                              | storage for                       | to meet following       | CR1.14(1)             |
|                              | c                                 | requirements1. Access   |                       |
|                              | ryptographic                      | authenticators stored   |                       |
|                              | keys/au                           | in secure HW2.          |                       |
|                              | thenticators                      | Symmetric keys stored   |                       |
|                              |                                   | in secure HW3. Private  |                       |
|                              |                                   | keys stored in secure   |                       |
|                              |                                   | HW                      |                       |
+------------------------------+-----------------------------------+-------------------------+-----------------------+
| #REQ-CIP-SEC-IEC-FUNC-REQ_6  | PKI(Public                        | System should support   | IEC-62443-4-2         |
|                              | Key                               | following functionality | CR1.8,                |
|                              | Inf                               | 1. Usage of PKI for     | CR1.9,                |
|                              | rastructure)                      | authentication2.        | CR1.10,               |
|                              | support, use                      | Authenticator           | CR1.14,               |
|                              | of                                | feedback3. Strong       | CR3.1,                |
|                              | cryptography                      | symmetric key based     | CR3.1(1),             |
|                              |                                   | authentication4. All    | CR3.4,                |
|                              |                                   | communication protected | CR3.4(1),             |
|                              |                                   | by integrity and        | CR4.1, CR4.3          |
|                              |                                   | authentication5.        |                       |
|                              |                                   | Information             |                       |
|                              |                                   | confidentiality by      |                       |
|                              |                                   | applying encryption6.   |                       |
|                              |                                   | Key management          |                       |
|                              |                                   | according to NIST SP    |                       |
|                              |                                   | 800-577. Usage of       |                       |
|                              |                                   | cryptographic           |                       |
|                              |                                   | algorithms recognized   |                       |
|                              |                                   | by international        |                       |
|                              |                                   | standards               |                       |
+------------------------------+-----------------------------------+-------------------------+-----------------------+
| #REQ-CIP-SEC-IEC-FUNC-REQ_7  | A                                 | System should support   | IEC-62443-4-2         |
|                              | uthorization                      | following               | CR2.1,                |
|                              | enforcement                       | functionality1.         | CR2.1(1),             |
|                              | support                           | Authorization           | CR2.1(2),             |
|                              |                                   | enforcement for all     | CR3.9                 |
|                              |                                   | authenticated and       |                       |
|                              |                                   | identified users2.      |                       |
|                              |                                   | Usage of control        |                       |
|                              |                                   | policies like identity  |                       |
|                              |                                   | based policies, role    |                       |
|                              |                                   | based policies and rule |                       |
|                              |                                   | based policies          |                       |
+------------------------------+-----------------------------------+-------------------------+-----------------------+
| #REQ-CIP-SEC-IEC-FUNC-REQ_8  | Protection                        | System should support   | IEC-62443-4-2         |
|                              | of audit                          | protection of audit     | CR3.9                 |
|                              | information                       | information, audit logs |                       |
|                              |                                   | and all the audit tools |                       |
|                              |                                   | from unauthorized       |                       |
|                              |                                   | access, modifications,  |                       |
|                              |                                   | deletions               |                       |
+------------------------------+-----------------------------------+-------------------------+-----------------------+
| #REQ-CIP-SEC-IEC-FUNC-REQ_9  | Supervisor                        | System should support   | IEC-62443-4-2         |
|                              | override                          | functionality to        | CR2.1(3)              |
|                              |                                   | temporarily elevate     |                       |
|                              |                                   | privileges of a normal  |                       |
|                              |                                   | user to higher level,   |                       |
|                              |                                   | this should be recorded |                       |
|                              |                                   | and controllable        |                       |
+------------------------------+-----------------------------------+-------------------------+-----------------------+
| #REQ-CIP-SEC-IEC-FUNC-REQ_10 | Session lock                      | System should provide   | IEC-62443-4-2         |
|                              | and remote                        | following functionality | CR2.5, CR2.6          |
|                              | session                           | in order to protect     |                       |
|                              | termination                       | user sessions1.         |                       |
|                              |                                   | Unattended open         |                       |
|                              |                                   | sessions should get     |                       |
|                              |                                   | locked after a          |                       |
|                              |                                   | configurable time       |                       |
|                              |                                   | period and either user  |                       |
|                              |                                   | or admin should be able |                       |
|                              |                                   | to unlock2. Terminate   |                       |
|                              |                                   | remote session after    |                       |
|                              |                                   | configurable period of  |                       |
|                              |                                   | time                    |                       |
+------------------------------+-----------------------------------+-------------------------+-----------------------+
| #REQ-CIP-SEC-IEC-FUNC-REQ_11 | Auditable                         | System should support   | IEC-62443-4-2         |
|                              | events                            | following functionality | CR2.8,                |
|                              |                                   | for auditable events1.  | CR2.10,               |
|                              |                                   | Create audit records    | CR2.12,               |
|                              |                                   | for security events     | CR2.12(1)             |
|                              |                                   | such as access control, |                       |
|                              |                                   | request errors, control |                       |
|                              |                                   | system events,          |                       |
|                              |                                   | backup-restore events,  |                       |
|                              |                                   | configuration changes,  |                       |
|                              |                                   | audit log events2. Each |                       |
|                              |                                   | record should have      |                       |
|                              |                                   | timestamp, source,      |                       |
|                              |                                   | event id, event         |                       |
|                              |                                   | results3. Automated     |                       |
|                              |                                   | notification of         |                       |
|                              |                                   | integrity violation     |                       |
+------------------------------+-----------------------------------+-------------------------+-----------------------+
| #REQ-CIP-SEC-IEC-FUNC-REQ_12 | Audit                             | System should support   | IEC-62443-4-2         |
|                              | storage                           | following functionality | CR2.9,                |
|                              |                                   | for audit storage1.     | CR2.9(1)              |
|                              |                                   | Allocate audit record   |                       |
|                              |                                   | storage according to    |                       |
|                              |                                   | recommended size2.      |                       |
|                              |                                   | Mechanism to protect    |                       |
|                              |                                   | against failure of      |                       |
|                              |                                   | component when it       |                       |
|                              |                                   | reaches audit storage   |                       |
|                              |                                   | limit3. Give warning    |                       |
|                              |                                   | when audit record       |                       |
|                              |                                   | storage capacity        |                       |
|                              |                                   | threshold reached       |                       |
+------------------------------+-----------------------------------+-------------------------+-----------------------+
| #REQ-CIP-SEC-IEC             | Software and                      | System should provide   | IEC-62443-4-2         |
| -FUNC-REQ_13                 | information                       | following functionality | CR3.4(1),             |
|                              | integrity                         | to support this         | CR3.4(2)              |
|                              |                                   | requirement1.           |                       |
|                              |                                   | Capability to detect    |                       |
|                              |                                   | authenticity of         |                       |
|                              |                                   | software and            |                       |
|                              |                                   | information2. Report    |                       |
|                              |                                   | integrity violations in |                       |
|                              |                                   | an automated fashion    |                       |
+------------------------------+-----------------------------------+-------------------------+-----------------------+
| #REQ-CIP-SEC-IEC             | Denial of                         | System should support   | IEC-62443-4-2         |
| -FUNC-REQ_14                 | service                           | mitigation for DoS      | CR7.1(1)              |
|                              | protection                        | attacks and make sure   |                       |
|                              |                                   | essential services are  |                       |
|                              |                                   | kept intact             |                       |
+------------------------------+-----------------------------------+-------------------------+-----------------------+
| #REQ-CIP-SEC-IEC-FUNC-REQ_15 | Control                           | System should support   | IEC-62443-4-2         |
|                              | system                            | following functions1.   | CR7.3,                |
|                              | backup and                        | Capability to support   | CR7.3(1)              |
|                              | recovery                          | system level backup     |                       |
|                              |                                   | including system and    |                       |
|                              |                                   | user state without      |                       |
|                              |                                   | affecting normal        |                       |
|                              |                                   | operation2. Backup      |                       |
|                              |                                   | information should be   |                       |
|                              |                                   | encrypted in order to   |                       |
|                              |                                   | safeguard it, backup    |                       |
|                              |                                   | information should not  |                       |
|                              |                                   | store encryption keys   |                       |
|                              |                                   | instead encryption keys |                       |
|                              |                                   | should be backed up     |                       |
|                              |                                   | separately3. The        |                       |
|                              |                                   | integrity check of back |                       |
|                              |                                   | up data should be       |                       |
|                              |                                   | supported               |                       |
+------------------------------+-----------------------------------+-------------------------+-----------------------+
| #REQ-CIP-SEC-IEC-FUNC-REQ_16 | Timestamp                         | System should support   | IEC-62443-4-2         |
|                              | and time                          | following functions1.   | CR2.11,               |
|                              | syn                               | Capability to create    | CR2.11(1)             |
|                              | chronization                      | timestamps that can be  |                       |
|                              |                                   | used in logs, audit     |                       |
|                              |                                   | records and other       |                       |
|                              |                                   | required places2.       |                       |
|                              |                                   | Timestamps are          |                       |
|                              |                                   | synchronized system     |                       |
|                              |                                   | wide with a common      |                       |
|                              |                                   | source3. Time           |                       |
|                              |                                   | synchronization         |                       |
|                              |                                   | mechanism should be     |                       |
|                              |                                   | protected in such a way |                       |
|                              |                                   | that any alteration     |                       |
|                              |                                   | could be detected       |                       |
+------------------------------+-----------------------------------+-------------------------+-----------------------+
| #REQ-CIP-SEC-IEC-FUNC-REQ_17 | Information persistence           |System should support    |IEC-62443-4-2 CR4.2    |
|                              |                                   |  following functions<br>|                       |
|                              |                                   |  1. Capability to erase |                       |
|                              |                                   |  information, for which |                       |
|                              |                                   |  explicit read          |                       |
|                              |                                   |  authorization is       |                       |
|                              |                                   |  supported<br>          |                       |
|                              |                                   |  (aka factory reset)    |                       |
|                              |                                   |                         |                       |
+------------------------------+-----------------------------------+-------------------------+-----------------------+

Other Security Requirements 
---------------------------

File Integrity 
~~~~~~~~~~~~~~

+-----------------+-----------------+-----------------+-----------------+
| Requirement ID  | Requirement     | Description     | Source of       |
|                 |                 |                 | requirement     |
+=================+=================+=================+=================+
| #               | Integrity of    | Receiver of CIP | IEC-62443-4-1   |
| REQ-CIP-SEC-IEC | CIP source      | shall be able   | SM-6            |
| -NON-FUNC-REQ_1 | code, scripts,  | to verify       |                 |
|                 | executable      | integrity of    |                 |
|                 |                 | scripts, source |                 |
|                 |                 | code and        |                 |
|                 |                 | executable      |                 |
+-----------------+-----------------+-----------------+-----------------+

Development Environment Security 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

+-----------------+-----------------+-----------------+-----------------+
| Requirement ID  | Requirement     | Description     | Source of       |
|                 |                 |                 | requirement     |
+=================+=================+=================+=================+
| #               | Development     | There shall be  | IEC-62443-4-1   |
| REQ-CIP-SEC-IEC | environment     | a process to    | SM-7            |
| -NON-FUNC-REQ_2 | security        | ensure          |                 |
|                 |                 | protection of   |                 |
|                 |                 | product during, |                 |
|                 |                 | development,    |                 |
|                 |                 | production and  |                 |
|                 |                 | delivery        |                 |
|                 |                 | including       |                 |
|                 |                 | software        |                 |
|                 |                 | updates during  |                 |
|                 |                 | design,         |                 |
|                 |                 | implementation, |                 |
|                 |                 | testing and     |                 |
|                 |                 | release         |                 |
+-----------------+-----------------+-----------------+-----------------+

Private Key Protection 
~~~~~~~~~~~~~~~~~~~~~~

+-----------------+-----------------+-----------------+-----------------+
| Requirement ID  | Requirement     | Description     | Source of       |
|                 |                 |                 | requirement     |
+=================+=================+=================+=================+
| #               | Protection of   | There shall be  | IEC-62443-4-1   |
| REQ-CIP-SEC-IEC | private keys    | a process to    | SM-8            |
| -NON-FUNC-REQ_3 |                 | protect all     |                 |
|                 |                 | private keys    |                 |
|                 |                 | used for code   |                 |
|                 |                 | signing from    |                 |
|                 |                 | unauthorized    |                 |
|                 |                 | access or       |                 |
|                 |                 | modifications   |                 |
+-----------------+-----------------+-----------------+-----------------+

CIP Core CVE Tracking 
~~~~~~~~~~~~~~~~~~~~~

**#REQ-CIP-SEC-IEC-NON-FUNC-REQ_4**

CIP Core should define and implement CVE tracking methods to incorporate
latest fixes for CVEs

CIP Kernel CVE Tracking 
~~~~~~~~~~~~~~~~~~~~~~~

**#REQ-CIP-SEC-IEC-NON-FUNC-REQ_5**

CIP Kernel should define and implement CVE tracking methods to
incorporate latest fixes for CVEs

Security Level 
~~~~~~~~~~~~~~

**#REQ-CIP-SEC-IEC-NON-FUNC-REQ_6**

CIP targets to achieve SL-3 by adding required security features and configurations. 
Note, that the adherence to the IEC-62443-4-1 process requirements is not dependent
on any specific security level. In other words, adherence to IEC-62443-4-1 must be
complete for any security level.

Security Updates 
~~~~~~~~~~~~~~~~

**#REQ-CIP-SEC-IEC-NON-FUNC-REQ_7**

CIP should apply latest security fixes published through CVEs.

Default User Accounts 
~~~~~~~~~~~~~~~~~~~~~

**#REQ-CIP-SEC-IEC-NON-FUNC-REQ_8**

CIP should publish default user accounts and their privileges.

Security Context 
~~~~~~~~~~~~~~~~

**#REQ-CIP-SEC-IEC-NON-FUNC-REQ_9**

Product owners should define product security context as expected in
IEC-62443-4-1 SR-1.

The security context shall describe:

* scope of the product
* a system overview (including high level breakdown of components)
* description of the intended operational environment
* assumptions about the product and the environment and constraints of the product
* expected measures to be available for threat mitigation
* the targeted network zone
* physical protection requirements and assumptions
* recommended firewall setup

The purpose of the defined security setup is to ensure that product developers and
product users have a shared understanding of the security expectations in the
intended operational environment and to ensure that product users understand the
security implications of the product's exposure within its ecosystem.
