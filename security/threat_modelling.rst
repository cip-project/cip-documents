CIP Threat Modeling
===================

.. contents::

.. list-table:: Revision History
   :header-rows: 1
                              
   * - Revision No 
     - Date
     - Change description 
     - Author  
     - Reviewed by

   * - 001
     - 2021-03-14 
     - Draft Threat Modeling Document 
     - Dinesh Kumar 
     - TBR

   * - 002  
     - 2021-12-21
     - Updated in SWG meeting. 
     - Yasin User
     - SWG


1. Objective 
------------

The primary objective of this document is to create Threat Model for CIP
reference platform. This Threat Model document can be re-used by CIP
member companies or end product owners and identify potential threats
for the end product. End products may have different business goals and
types of risks, accordingly Threats should be identified.

Since the main goal of CIP is to maintain Open Source Base Layer (OSBL)
for long term by re-using existing open source resources, as a result
core design of the platform will remain same, therefore CIP threat model
will not depend upon any specific versions of Debian or CIP Kernel.
However, the period and condition of reviewing CIP Threat Model will be
defined in later part of this document.

Moreover, subsequent revisions of this document may consider additional
details of existing scenarios or address newly reported security issues.

2. Assumptions 
--------------

+-----------------------------------------------------------+----------+
| Assumption                                                | Impact   |
+===========================================================+==========+
| CIP threat model is based on generic use cases of CIP     | None     |
| reference platform                                        |          |
+-----------------------------------------------------------+----------+

3. Scope 
--------

Scope of this document is to consider only generic use cases for CIP for
Embedded and Networking Categories as well as CIP development scenarios.

4. Security Requirements 
------------------------

CIP Security requirements are defined in CIP Security Requirements
document

Current Security requirements have been defined based on IEC-62443-4-2
security requirements as there were no specific security requirements
shared by CIP members.

5. Threat Modeling Strategy 
---------------------------

CIP Threat Modeling would be primarily based on following strategies

**STRIDE**

STRIDE will be used for analyzing key CIP development and CIP use cases
scenarios.

**Attack Trees**

There are some scenarios which will be covered by using attack trees.

**CVSS**

Since CIP Core and CIP Kernel already uses CVE scanner and automatically
apply fixes to open CVEs, CVSS is inherently used by CIP.

In addition to above mentioned methodologies, whenever some threat is
identified by some use case or found in upstream, same can be
incorporated to keep the Threat Model up-to-date.

6. Data Flow Diagrams(DFD) 
--------------------------

This section will have multiple Data Flow Diagrams(DFDs) for various use
case scenarios. Various scenarios for data flows in `process
view <https://en.wikipedia.org/wiki/4%2B1_architectural_view_model>`__
and development view have been considered

6.1 Development View 
~~~~~~~~~~~~~~~~~~~~

6.1.1 CIP Development Context Diagram 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Following diagram illustrates CIP context diagram which highlights
external entities which will interact with CIP platform during
development.

**Assumptions** All external entities are authenticated while
interacting with CIP development environment.

**Note**

Threat IDs are generated as follows

Threat\_\_ID

-  Here section_no refers to section in this document

-  ID refers to the ID generated in Threat Modeling Tool

.. figure:: ../resources/images/threat_modeling/CIP_Context_diagram.png
   :alt: CIP Development Context Diagram

   CIP Development Context Diagram

+----------------+--------------------+------------+-------------+------------+
| Threat ID      | Threats            | Category   | Remarks     | Mitigation | 
|                | Identified         |            |             |            |
+================+====================+============+=============+============+
| Threat_6.1.1_1 | CIP                | Elevation  | A user      | Not        |
|                | Platform           | of         | gains       | applicable |
|                | may be able        | privileges | increased   |            |
|                | to                 |            | capability  |            |
|                | impersonate        |            | or          |            |
|                | the context        |            | privilege   |            |
|                | of Other           |            | by taking   |            |
|                | OSS                |            | advantage   |            |
|                | developers         |            | of          |            |
|                | in order to        |            | imp         |            |
|                | gain               |            | lementation |            |
|                | additional         |            | bug         |            |
|                | privilege.         |            |             |            |
+----------------+--------------------+------------+-------------+------------+


6.1.2 CIP Kernel Development 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Following data flow diagram illustrates CIP Kernel development and how
various external entities make changes in the CIP code.

**Assumptions**

Only CIP Kernel maintainers have merge privileges, all other developers
can only send merger request, it’s up to CIP Kernel maintainer to accept
or reject it.

.. raw:: pdf

   PageBreak

.. figure:: ../resources/images/threat_modeling/CIP_Kernel_Development.png
   :alt: CIP Kernel Development DFD

   CIP Kernel Development DFD

+-----------------+--------------------+-------------+-------------+------------+
| Threat ID       | Threats            | Category    | Remarks     | Mitigation | 
|                 | Identified         |             |             |            |
+=================+====================+=============+=============+============+
| Threat_6.1.2_5  | Spoofing of        | Spoofing    | Mainline    |            |
|                 | Source Data        |             | Kernel repo |            |
|                 | Store              |             | may be      |            |
|                 | Mainline           |             | spoofed by  |            |
|                 | Kernel repo        |             | an attacker |            |
|                 |                    |             | and this    |            |
|                 |                    |             | may lead to |            |
|                 |                    |             | incorrect   |            |
|                 |                    |             | data        |            |
|                 |                    |             | delivered   |            |
|                 |                    |             | to CIP      |            |
|                 |                    |             | Kernel      |            |
|                 |                    |             | Maintainer  |            |
+-----------------+--------------------+-------------+-------------+------------+
| Threat_6.1.2_7  | Aut                | Repudiation | Consider    |            |
|                 | hentication        |             | using       |            |
|                 | Service            |             | logging or  |            |
|                 | claims that        |             | auditing to |            |
|                 | it did not         |             | record the  |            |
|                 | receive            |             | source,     |            |
|                 | data from a        |             | time, and   |            |
|                 | source             |             | summary of  |            |
|                 | outside the        |             | the         |            |
|                 | trust              |             | received    |            |
|                 | boundary           |             | data.       |            |
+-----------------+--------------------+-------------+-------------+------------+
| Threat_6.1.2_18 | Weak Access        | Information | Review      |            |
|                 | Control for        | disclosure  | au          |            |
|                 | a Resource         |             | thorization |            |
|                 |                    |             | settings.   |            |
+-----------------+--------------------+-------------+-------------+------------+
| Threat_6.1.2_41 | Aut                | Elevation   | Each CIP    |            |
|                 | hentication        | of          | member      |            |
|                 | Service May        | privileges  | companies   |            |
|                 | be Subject         |             | member      |            |
|                 | to                 |             | should have |            |
|                 | Elevation          |             | right       |            |
|                 | of                 |             | privileges  |            |
|                 | Privilege          |             |             |            |
|                 | Using              |             |             |            |
|                 | Remote Code        |             |             |            |
|                 | Execution          |             |             |            |
+-----------------+--------------------+-------------+-------------+------------+
| Threat_6.1.2_42 | Spoofing of        | Spoofing    | Use highest |            |
|                 | the CIP            |             | available   |            |
|                 | Member             |             | login       |            |
|                 | Companies          |             | security    |            |
|                 | External           |             | mechanism   |            |
|                 | Destination        |             | such as 2F  |            |
|                 | Entity             |             | aut         |            |
|                 |                    |             | hentication |            |
+-----------------+--------------------+-------------+-------------+------------+
| Threat_6.1.2_49 | Weak Access        | Information | Review      |            |
|                 | Control for        | disclosure  | au          |            |
|                 | a Resource         |             | thorization |            |
|                 |                    |             | settings of |            |
|                 |                    |             | CIP Kernel  |            |
|                 |                    |             | git repo    |            |
+-----------------+--------------------+-------------+-------------+------------+
| Threat_6.1.2_62 | Spoofing of        | Spoofing    | Consider    |            |
|                 | Destination        |             | using a     |            |
|                 | Data Store         |             | standard    |            |
|                 | CIP Kernel         |             | aut         |            |
|                 | git repo           |             | hentication |            |
|                 |                    |             | mechanism   |            |
|                 |                    |             | to identify |            |
|                 |                    |             | the         |            |
|                 |                    |             | destination |            |
|                 |                    |             | data store. |            |
+-----------------+--------------------+-------------+-------------+------------+
| Threat_6.1.2_63 | The CIP            | Tempering   | Ensure the  |            |
|                 | Kernel git         |             | integrity   |            |
|                 | repo Data          |             | of the data |            |
|                 | Store Could        |             | flow to the |            |
|                 | Be                 |             | data store. |            |
|                 | Corrupted          |             |             |            |
+-----------------+--------------------+-------------+-------------+------------+
| Threat_6.1.2_67 | Au                 | Tempering   | An attacker |            |
|                 | thenticated        |             | can read or |            |
|                 | Data Flow          |             | modify data |            |
|                 | Compromised        |             | transmitted |            |
|                 |                    |             | over an     |            |
|                 |                    |             | au          |            |
|                 |                    |             | thenticated |            |
|                 |                    |             | data flow.  |            |
|                 |                    |             | send data   |            |
|                 |                    |             | encrypted   |            |
+-----------------+--------------------+-------------+-------------+------------+
| Threat_6.1.2_68 | Spoofing of        | Spoofing    | Consider    |            |
|                 | Destination        |             | using a     |            |
|                 | Data Store         |             | standard    |            |
|                 | Mainline           |             | aut         |            |
|                 | Kernel repo        |             | hentication |            |
|                 |                    |             | mechanism   |            |
|                 |                    |             | to identify |            |
|                 |                    |             | the         |            |
|                 |                    |             | destination |            |
|                 |                    |             | data store. |            |
+-----------------+--------------------+-------------+-------------+------------+
| Threat_6.1.2_70 | Weak Access        | Information | Review      |            |
|                 | Control for        | disclosure  | au          |            |
|                 | a Resource         |             | thorization |            |
|                 |                    |             | settings.   |            |
+-----------------+--------------------+-------------+-------------+------------+


6.1.3 CIP Core Development 
^^^^^^^^^^^^^^^^^^^^^^^^^^

Following diagram illustrates CIP Core development for isar and Deby.
CIP Core has only meta-data and recipes as well as build tools. Actual
package source code or binary packages are downloaded from Debian repos
while creating CIP images.

.. raw:: pdf

   PageBreak

.. figure:: ../resources/images/threat_modeling/CIP_Core_Development.png
   :alt: CIP Core Development DFD

   CIP Core Development DFD

+-----------------+--------------------+-------------+-------------+------------+
| Threat ID       | Threats            | Category    | Remarks     | Mitigation | 
|                 | Identified         |             |             |            |
+=================+====================+=============+=============+============+
| Threat_6.1.3_79 | Data Flow          | Denial of   | An external |            |
|                 | HTTPS Is           | Service     | agent       |            |
|                 | Potentially        |             | interrupts  |            |
|                 | Interrupted        |             | data        |            |
|                 |                    |             | flowing     |            |
|                 |                    |             | across a    |            |
|                 |                    |             | trust       |            |
|                 |                    |             | boundary in |            |
|                 |                    |             | either      |            |
|                 |                    |             | di          |            |
|                 |                    |             | rection.Use |            |
|                 |                    |             | aut         |            |
|                 |                    |             | hentication |            |
|                 |                    |             | and send    |            |
|                 |                    |             | encrypted   |            |
|                 |                    |             | data        |            |
+-----------------+--------------------+-------------+-------------+------------+
| Threat_6.1.3_84 | Spoofing of        | Spoofing    | Consider    |            |
|                 | the CIP            |             | using a     |            |
|                 | Core               |             | standard    |            |
|                 | Maintainer         |             | aut         |            |
|                 | External           |             | hentication |            |
|                 | Destination        |             | mechanism   |            |
|                 | Entity             |             | to identify |            |
|                 |                    |             | the         |            |
|                 |                    |             | external    |            |
|                 |                    |             | entity.     |            |
+-----------------+--------------------+-------------+-------------+------------+
| Threat_6.1.3_87 | Spoofing of        | Spoofing    | Consider    |            |
|                 | Destination        |             | using a     |            |
|                 | Data Store         |             | standard    |            |
|                 | CIP Core           |             | aut         |            |
|                 | repo               |             | hentication |            |
|                 |                    |             | mechanism   |            |
|                 |                    |             | to identify |            |
|                 |                    |             | the         |            |
|                 |                    |             | destination |            |
|                 |                    |             | data store. |            |
+-----------------+--------------------+-------------+-------------+------------+
| Threat_6.1.3_88 | The CIP            | Tempering   | Ensure the  |            |
|                 | Core repo          |             | integrity   |            |
|                 | Data Store         |             | of the data |            |
|                 | Could Be           |             | flow to the |            |
|                 | Corrupted          |             | data store. |            |
+-----------------+--------------------+-------------+-------------+------------+
| Threat_6.1.3_92 | Spoofing of        | Spoofing    | Consider    |            |
|                 | Destination        |             | using a     |            |
|                 | Data Store         |             | standard    |            |
|                 | CIP Core           |             | aut         |            |
|                 | repo               |             | hentication |            |
|                 |                    |             | mechanism   |            |
|                 |                    |             | to identify |            |
|                 |                    |             | the         |            |
|                 |                    |             | destination |            |
|                 |                    |             | data store. |            |
+-----------------+--------------------+-------------+-------------+------------+
| Threat_6.1.3_97 | Spoofing of        | Spoofing    | Consider    |            |
|                 | Source Data        |             | using a     |            |
|                 | Store CIP          |             | standard    |            |
|                 | Core repo          |             | aut         |            |
|                 |                    |             | hentication |            |
|                 |                    |             | mechanism   |            |
|                 |                    |             | to identify |            |
|                 |                    |             | the source  |            |
|                 |                    |             | data store. |            |
+-----------------+--------------------+-------------+-------------+------------+
| Threat_6.1.3_98 | External           | Repudiation | Consider    |            |
|                 | Entity CIP         |             | using       |            |
|                 | Core               |             | logging or  |            |
|                 | Developer          |             | auditing to |            |
|                 | Potentially        |             | record the  |            |
|                 | Denies             |             | source,     |            |
|                 | Receiving          |             | time, and   |            |
|                 | Data               |             | summary of  |            |
|                 |                    |             | the         |            |
|                 |                    |             | received    |            |
|                 |                    |             | data.       |            |
+-----------------+--------------------+-------------+-------------+------------+
| Threat_6.1.3_99 | Weak Access        | Information | Review      |            |
|                 | Control for        | disclosure  | au          |            |
|                 | a Resource         |             | thorization |            |
|                 |                    |             | settings.   |            |
+-----------------+--------------------+-------------+-------------+------------+

6.1.5 CIP OS Image Creation 
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Following diagram illustrates data flow when CIP image is created. While
creating CIP image CIP Kernel source is downloaded as well as Debian
packages source or binaries. Once the image is created it is saved in
external storage such as AWS.

.. figure:: ../resources/images/threat_modeling/CIP_Image_Creation.png
   :alt: CIP Image Creation DFD

   CIP Image Creation DFD

+------------------+--------------------+-------------+-----------------+
| Threat ID        | Threats            | Category    | Remarks         | 
|                  | Identified         |             |                 |
+==================+====================+=============+=================+
| Threat_6.1.5_102 | Spoofing of        | Spoofing    | Consider using  |
|                  | Source Data        |             | a standard      |
|                  | Store Debian       |             | authentication  |
|                  | upstream repo      |             | mechanism to    |
|                  |                    |             | identify the    |
|                  |                    |             | source data     |
|                  |                    |             | store.          |
+------------------+--------------------+-------------+-----------------+
| Threat_6.1.5_103 | Spoofing of        | Spoofing    | Consider using  |
|                  | Destination        |             | a standard      |
|                  | Data Store CIP     |             | authentication  |
|                  | Development        |             | mechanism to    |
|                  | Storage            |             | identify the    |
|                  |                    |             | destination     |
|                  |                    |             | data store.     |
+------------------+--------------------+-------------+-----------------+
| Threat_6.1.5_107 | Spoofing of        | Spoofing    | Consider using  |
|                  | Source Data        |             | a standard      |
|                  | Store CIP          |             | authentication  |
|                  | Kernel repo        |             | mechanism to    |
|                  |                    |             | identify the    |
|                  |                    |             | source data     |
|                  |                    |             | store.          |
+------------------+--------------------+-------------+-----------------+
| Threat_6.1.5_112 | Spoofing of        | Spoofing    | Consider using  |
|                  | Source Data        |             | a standard      |
|                  | Store CIP          |             | authentication  |
|                  | Development        |             | mechanism to    |
|                  | Storage            |             | identify the    |
|                  |                    |             | source data     |
|                  |                    |             | store.          |
+------------------+--------------------+-------------+-----------------+
| Threat_6.1.5_113 | Spoofing of        | Spoofing    | Consider using  |
|                  | Destination        |             | a standard      |
|                  | Data Store CIP     |             | authentication  |
|                  | Image storage      |             | mechanism to    |
|                  |                    |             | identify the    |
|                  |                    |             | destination     |
|                  |                    |             | data store      |
+------------------+--------------------+-------------+-----------------+
| Threat_6.1.5_114 | Data Store         | Repudiation | Consider using  |
|                  | Denies CIP         |             | logging or      |
|                  | Image storage      |             | auditing to     |
|                  | Potentially        |             | record the      |
|                  | Writing Data       |             | source, time,   |
|                  |                    |             | and summary of  |
|                  |                    |             | the received    |
|                  |                    |             | data.           |
+------------------+--------------------+-------------+-----------------+
| Threat_6.1.5_116 | Data Store         | Denial of   | An external     |
|                  | Inaccessible       | Service     | agent prevents  |
|                  |                    |             | access to a     |
|                  |                    |             | data store on   |
|                  |                    |             | the other side  |
|                  |                    |             | of the trust    |
|                  |                    |             | boundary.       |
+------------------+--------------------+-------------+-----------------+
| T                | Spoofing of        | Spoofing    | Consider using  |
| hreat_6.1.5_122  | Source Data        |             | a standard      |
|                  | Store CIP          |             | authentication  |
|                  | Kernel repo        |             | mechanism to    |
|                  |                    |             | identify the    |
|                  |                    |             | source data     |
|                  |                    |             | store           |
+------------------+--------------------+-------------+-----------------+
| Threat_6.1.5_123 | Weak Access        | Information | Review          |
|                  | Control for a      | disclosure  | authorization   |
|                  | Resource           |             | settings        |
+------------------+--------------------+-------------+-----------------+
| Threat_6.1.5_126 | Spoofing of        | Information | Consider using  |
|                  | Destination        | disclosure  | a standard      |
|                  | Data Store CIP     |             | authentication  |
|                  | Image storage      |             | mechanism to    |
|                  |                    |             | identify the    |
|                  |                    |             | destination     |
|                  |                    |             | data store.     |
+------------------+--------------------+-------------+-----------------+
| Threat_6.1.5_130 | The CIP            | Tempering   | Ensure the      |
|                  | Development        |             | integrity of    |
|                  | Storage Data       |             | the data flow   |
|                  | Store Could Be     |             | to the data     |
|                  | Corrupted          |             | store.          |
+------------------+--------------------+-------------+-----------------+
| Threat_6.1.5_215 | Potential          | Denial of   | CIP gitlab CI   |
|                  | Process Crash      | Service     | crashes, halts, |
|                  | or Stop for        |             | stops or runs   |
|                  | gitlab CI          |             | slowly; in all  |
|                  |                    |             | cases violating |
|                  |                    |             | an availability |
|                  |                    |             | metric.         |
+------------------+--------------------+-------------+-----------------+
| Threat_6.1.5_226 | The CIP Image      | Tempering   | Ensure the      |
|                  | storage Data       |             | integrity of    |
|                  | Store Could Be     |             | the data flow   |
|                  | Corrupted          |             | to the data     |
|                  |                    |             | store.          |
+------------------+--------------------+-------------+-----------------+
| Threat_6.1.5_227 | Data Store         | Repudiation | Consider using  |
|                  | Denies CIP         |             | logging or      |
|                  | Image storage      |             | auditing to     |
|                  | Potentially        |             | record the      |
|                  | Writing Data       |             | source, time,   |
|                  |                    |             | and summary of  |
|                  |                    |             | the received    |
|                  |                    |             | data.           |
+------------------+--------------------+-------------+-----------------+

6.2 Process View 
^^^^^^^^^^^^^^^^

6.2.1 CIP as networking switch 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Following diagram depicts a CIP use case when Networking Switch is
developed using CIP platform.

.. figure:: ../resources/images/threat_modeling/CIP_As_Networking_Switch.png
   :alt: CIP As Networking Use Case DFD

   CIP As Networking Use Case DFD

Following threats table list all the threats identified using DFD.

+------------------+--------------------+-----------------+-----------------+
| Threat ID        | Threats            | Category        | Remarks         | 
|                  | Identified         |                 |                 |
+==================+====================+=================+=================+
| Threat_6.2.1_146 | Potential Data     | Repudiation     | Consider using  |
|                  | Repudiation by     |                 | logging or      |
|                  | Store & Forward    |                 | auditing to     |
|                  |                    |                 | record the      |
|                  |                    |                 | source, time,   |
|                  |                    |                 | and summary of  |
|                  |                    |                 | the received    |
|                  |                    |                 | data.           |
+------------------+--------------------+-----------------+-----------------+
| Threat_6.2.1_148 | Potential          | Denial of       | Store & Forward |
|                  | Process Crash      | service         | crashes, halts, |
|                  | or Stop for        |                 | stops or runs   |
|                  | Store & Forward    |                 | slowly; in all  |
|                  |                    |                 | cases violating |
|                  |                    |                 | an availability |
|                  |                    |                 | metric Needs    |
|                  |                    |                 | Investigation.  |
+------------------+--------------------+-----------------+-----------------+
| Threat_6.2.1_150 | Elevation Using    | Elevation using | Store & Forward |
|                  | Impersonation      | impersonation   | may be able to  |
|                  |                    |                 | impersonate the |
|                  |                    |                 | context of      |
|                  |                    |                 | DCS/PLC in      |
|                  |                    |                 | order to gain   |
|                  |                    |                 | additional      |
|                  |                    |                 | privilege.      |
+------------------+--------------------+-----------------+-----------------+
| Threat_6.2.1_151 | Store & Forward    | Elevation of    | DCS/PLC may be  |
|                  | May be Subject     | privileges      | able to         |
|                  | to Elevation of    |                 | remotely        |
|                  | Privilege Using    |                 | execute code    |
|                  | Remote Code        |                 | for Store &     |
|                  | Execution          |                 | Forward.        |
+------------------+--------------------+-----------------+-----------------+
| Threat_6.2.1_152 | Elevation by       | Elevation of    | An attacker may |
|                  | Changing the       | privileges      | pass data into  |
|                  | Execution Flow     |                 | Store & Forward |
|                  | in Store &         |                 | in order to     |
|                  | Forward            |                 | change the flow |
|                  |                    |                 | of program      |
|                  |                    |                 | execution       |
|                  |                    |                 | within Store &  |
|                  |                    |                 | Forward to the  |
|                  |                    |                 | attacker’s      |
|                  |                    |                 | choosing.       |
+------------------+--------------------+-----------------+-----------------+
| Threat_6.2.1_155 | Spoofing of the    | Spoofing        | Consider using  |
|                  | DCS/PLC            |                 | a standard      |
|                  | External           |                 | authentication  |
|                  | Destination        |                 | mechanism to    |
|                  | Entity             |                 | identify the    |
|                  |                    |                 | external        |
|                  |                    |                 | entity.         |
+------------------+--------------------+-----------------+-----------------+
| Threat_6.2.1_156 | External Entity    | Repudiation     | Consider using  |
|                  | DCS/PLC            |                 | logging or      |
|                  | Potentially        |                 | auditing to     |
|                  | Denies             |                 | record the      |
|                  | Receiving Data     |                 | source, time,   |
|                  |                    |                 | and summary of  |
|                  |                    |                 | the received    |
|                  |                    |                 | data.           |
+------------------+--------------------+-----------------+-----------------+
| Threat_6.2.1_163 | Potential          | Denial of       | Store & Forward |
|                  | Process Crash      | service         | crashes, halts, |
|                  | or Stop for        |                 | stops or runs   |
|                  | Store & Forward    |                 | slowly; in all  |
|                  |                    |                 | cases violating |
|                  |                    |                 | an availability |
|                  |                    |                 | metric.Needs    |
|                  |                    |                 | Investigation.  |
+------------------+--------------------+-----------------+-----------------+
| Threat_6.2.1_164 | Data Flow          | Denial of       | An external     |
|                  | Binary Is          | service         | agent           |
|                  | Potentially        |                 | interrupts data |
|                  | Interrupted        |                 | flowing across  |
|                  |                    |                 | a trust         |
|                  |                    |                 | boundary in     |
|                  |                    |                 | either          |
|                  |                    |                 | direction.Needs |
|                  |                    |                 | Investigation.  |
+------------------+--------------------+-----------------+-----------------+
| Threat_6.2.1_166 | Store & Forward    | Elevation of    | ESD may be able |
|                  | May be Subject     | privileges      | to remotely     |
|                  | to Elevation of    |                 | execute code    |
|                  | Privilege Using    |                 | for Store &     |
|                  | Remote Code        |                 | Forward.        |
|                  | Execution          |                 |                 |
+------------------+--------------------+-----------------+-----------------+
| Threat_6.2.1_173 | Spoofing of the    | Spoofing        | Consider using  |
|                  | Safety ES          |                 | a standard      |
|                  | External           |                 | authentication  |
|                  | Destination        |                 | mechanism to    |
|                  | Entity             |                 | identify the    |
|                  |                    |                 | external        |
|                  |                    |                 | entity.         |
+------------------+--------------------+-----------------+-----------------+
| Threat_6.2.1_176 | Authenticated      | Tempering       | An attacker can |
|                  | Data Flow          |                 | read or modify  |
|                  | Compromised        |                 | data            |
|                  |                    |                 | transmitted     |
|                  |                    |                 | over an         |
|                  |                    |                 | authenticated   |
|                  |                    |                 | dataflow.       |
+------------------+--------------------+-----------------+-----------------+
| Threat_6.2.1_182 | Weak Access        | Information     | Review          |
|                  | Control for a      | disclosure      | authorization   |
|                  | Resource           |                 | settings.       |
+------------------+--------------------+-----------------+-----------------+
| Threat_6.2.1_191 | Authorization      | Information     | Ensure that     |
|                  | Bypass             | disclosure      | your program is |
|                  |                    |                 | the only one    |
|                  |                    |                 | that can access |
|                  |                    |                 | the data, and   |
|                  |                    |                 | that all other  |
|                  |                    |                 | subjects have   |
|                  |                    |                 | to use your     |
|                  |                    |                 | interface.      |
+------------------+--------------------+-----------------+-----------------+
| Threat_6.2.1_195 | Weak Credential    | Information     | Credentials     |
|                  | Storage            | disclosure      | held at the     |
|                  |                    |                 | server are      |
|                  |                    |                 | often disclosed |
|                  |                    |                 | or tampered     |
|                  |                    |                 | with and        |
|                  |                    |                 | credentials     |
|                  |                    |                 | stored on the   |
|                  |                    |                 | client are      |
|                  |                    |                 | often stolen.   |
|                  |                    |                 | For server      |
|                  |                    |                 | side, consider  |
|                  |                    |                 | storing a       |
|                  |                    |                 | salted hash of  |
|                  |                    |                 | the credentials |
|                  |                    |                 | instead of      |
|                  |                    |                 | storing the     |
|                  |                    |                 | credentials     |
|                  |                    |                 | themselves. If  |
|                  |                    |                 | this is not     |
|                  |                    |                 | possible due to |
|                  |                    |                 | business        |
|                  |                    |                 | requirements,   |
|                  |                    |                 | be sure to      |
|                  |                    |                 | encrypt the     |
|                  |                    |                 | credentials     |
|                  |                    |                 | before storage, |
|                  |                    |                 | using an        |
|                  |                    |                 | SDL-approved    |
|                  |                    |                 | mechanism. For  |
|                  |                    |                 | client side, if |
|                  |                    |                 | storing         |
|                  |                    |                 | credentials is  |
|                  |                    |                 | required,       |
|                  |                    |                 | encrypt them    |
|                  |                    |                 | and protect the |
|                  |                    |                 | data store in   |
|                  |                    |                 | which they’re   |
|                  |                    |                 | stored          |
+------------------+--------------------+-----------------+-----------------+
| Threat_6.2.1_196 | Potential          | Denial of       | Resource        |
|                  | Excessive          | Service         | consumption     |
|                  | Resource           |                 | attacks can be  |
|                  | Consumption for    |                 | hard to deal    |
|                  | Authentication     |                 | with, and there |
|                  | for                |                 | are times that  |
|                  | configuration      |                 | it makes sense  |
|                  | or                 |                 | to let the OS   |
|                  | Configuration      |                 | do the job. Be  |
|                  | data store         |                 | careful that    |
|                  |                    |                 | your resource   |
|                  |                    |                 | requests don’t  |
|                  |                    |                 | deadlock, and   |
|                  |                    |                 | that they do    |
|                  |                    |                 | timeout.        |
+------------------+--------------------+-----------------+-----------------+
| Threat_6.2.1_234 | Data Flow          | Denial of       | An external     |
|                  | Authentication     | Service         | agent           |
|                  | status Is          |                 | interrupts data |
|                  | Potentially        |                 | flowing across  |
|                  | Interrupted        |                 | a trust         |
|                  |                    |                 | boundary in     |
|                  |                    |                 | either          |
|                  |                    |                 | direction.      |
+------------------+--------------------+-----------------+-----------------+
| Threat_6.2.1_236 | Potential          | Denial of       | Authentication  |
|                  | Process Crash      | Service         | for             |
|                  | or Stop for        |                 | configuration   |
|                  | Authentication     |                 | crashes, halts, |
|                  | for                |                 | stops or runs   |
|                  | configuration      |                 | slowly; in all  |
|                  |                    |                 | cases violating |
|                  |                    |                 | an availability |
|                  |                    |                 | metric.         |
+------------------+--------------------+-----------------+-----------------+
| Threat_6.2.1_238 | Elevation Using    | Elevation of    | Authentication  |
|                  | Impersonation      | privileges      | for             |
|                  |                    |                 | configuration   |
|                  |                    |                 | may be able to  |
|                  |                    |                 | impersonate the |
|                  |                    |                 | context of      |
|                  |                    |                 | Admin User in   |
|                  |                    |                 | order to gain   |
|                  |                    |                 | additional      |
|                  |                    |                 | privilege.      |
+------------------+--------------------+-----------------+-----------------+

6.2.2 CIP as PLC 
^^^^^^^^^^^^^^^^

Following diagram depicts a CIP use case using PLC.

.. figure:: ../resources/images/threat_modeling/CIP_based_PLC.png
   :alt: CIP based PLC

   CIP based PLC

+------------------+--------------------+--------------+-----------------+
| Threat ID        | Threats            | Category     | Remarks         | 
|                  | Identified         |              |                 |
+==================+====================+==============+=================+
| Threat_6.2.2_197 | Spoofing the       | Spoofing     | Consider using  |
|                  | PLC with           |              | a standard      |
|                  | communication      |              | authentication  |
|                  | interface          |              | mechanism to    |
|                  | Process            |              | identify the    |
|                  |                    |              | destination     |
|                  |                    |              | process.        |
+------------------+--------------------+--------------+-----------------+
| Threat_6.2.2_198 | Spoofing the       | Spoofing     | Consider using  |
|                  | Various sensors    |              | a standard      |
|                  | data,              |              | authentication  |
|                  | temperature,       |              | mechanism to    |
|                  | pressure           |              | identify the    |
|                  | External Entity    |              | external        |
|                  |                    |              | entity.         |
+------------------+--------------------+--------------+-----------------+
| Threat_6.2.2_200 | Potential Data     | Repudiation  | Consider using  |
|                  | Repudiation by     |              | logging or      |
|                  | PLC with           |              | auditing to     |
|                  | communication      |              | record the      |
|                  | interface          |              | source, time,   |
|                  |                    |              | and summary of  |
|                  |                    |              | the received    |
|                  |                    |              | data.           |
+------------------+--------------------+--------------+-----------------+
| Threat_6.2.2_201 | Data Flow          | Information  | Data flowing    |
|                  | Sniffing           | disclosure   | across Sensors  |
|                  |                    |              | data may be     |
|                  |                    |              | sniffed by an   |
|                  |                    |              | attacker.       |
|                  |                    |              | Depending on    |
|                  |                    |              | what type of    |
|                  |                    |              | data an         |
|                  |                    |              | attacker can    |
|                  |                    |              | read, it may be |
|                  |                    |              | used to attack  |
|                  |                    |              | other parts of  |
|                  |                    |              | the system or   |
|                  |                    |              | simply be a     |
|                  |                    |              | disclosure of   |
|                  |                    |              | information     |
|                  |                    |              | leading to      |
|                  |                    |              | compliance      |
|                  |                    |              | violations.     |
|                  |                    |              | Consider        |
|                  |                    |              | encrypting the  |
|                  |                    |              | data flow.      |
+------------------+--------------------+--------------+-----------------+
| Threat_6.2.2_202 | Potential          | Denial of    | PLC with        |
|                  | Process Crash      | Service      | communication   |
|                  | or Stop for PLC    |              | interface       |
|                  | with               |              | crashes, halts, |
|                  | communication      |              | stops or runs   |
|                  | interface          |              | slowly; in all  |
|                  |                    |              | cases violating |
|                  |                    |              | an availability |
|                  |                    |              | metric.         |
+------------------+--------------------+--------------+-----------------+
| Threat_6.2.2_204 | Elevation Using    | Elevation of | PLC with        |
|                  | Impersonation      | privileges   | communication   |
|                  |                    |              | interface may   |
|                  |                    |              | be able to      |
|                  |                    |              | impersonate the |
|                  |                    |              | context of      |
|                  |                    |              | Various sensors |
|                  |                    |              | data,           |
|                  |                    |              | temperature,    |
|                  |                    |              | pressure in     |
|                  |                    |              | order to gain   |
|                  |                    |              | additional      |
|                  |                    |              | privilege.      |
+------------------+--------------------+--------------+-----------------+
| Threat_6.2.2_208 | Spoofing of the    | Spoofing     | Consider using  |
|                  | Controlled         |              | a standard      |
|                  | processes          |              | authentication  |
|                  | External           |              | mechanism to    |
|                  | Destination        |              | identify the    |
|                  | Entity             |              | external        |
|                  |                    |              | entity.         |
+------------------+--------------------+--------------+-----------------+
| Threat_6.2.2_209 | External Entity    | Repudiation  | Consider using  |
|                  | Controlled         |              | logging or      |
|                  | processes          |              | auditing to     |
|                  | Potentially        |              | record the      |
|                  | Denies             |              | source, time,   |
|                  | Receiving Data     |              | and summary of  |
|                  |                    |              | the received    |
|                  |                    |              | data.           |
+------------------+--------------------+--------------+-----------------+

7. Potential Threats To the System and Mitigation 
-------------------------------------------------

This section will have consolidated list of threats identified from
various use cases and data flow scenarios. Example of mitigation could
be any of the following actions.

-  Add Debian packages to CIP
-  Provide Security Configurations
-  Provide Security Guidelines

+-------------------+---------------------+----------------+-----------------+
| Threat ID         | Use case /          | Impact         | Mitigation      | 
|                   | Scenario            |                |                 |
+===================+=====================+================+=================+
| Threat_6.2.1_146  | CIP based           | Repudiation    | CIP should      |
|                   | networking          |                | address this    |
|                   | switch has          |                | issue by        |
|                   | store & forward     |                | including       |
|                   | process, it         |                | logging or      |
|                   | claims that it      |                | auditing to     |
|                   | did not receive     |                | record the      |
|                   | data from a         |                | source, time,   |
|                   | source outside      |                | and summary of  |
|                   | the trust           |                | the received    |
|                   | boundary            |                | data            |
+-------------------+---------------------+----------------+-----------------+
| Threat_6.2.1_148, | CIP based           | Denial of      | Needs           |
| Threat_6.2.1_163, | networking          | service        | Investigation.  |
| Threat_6.2.1_164  | switch has          |                |                 |
|                   | store & forward     |                |                 |
|                   | process, it         |                |                 |
|                   | crashes, stops      |                |                 |
|                   | or runs slowly      |                |                 |
+-------------------+---------------------+----------------+-----------------+
| Threat_6.2.1_150  | CIP based           | Elevation of   | Authentication  |
|                   | networking          | using          | and             |
|                   | switch has          | impresonation  | authorization   |
|                   | store & forward     |                | of all external |
|                   | process, it may     |                | entities        |
|                   | be able to          |                |                 |
|                   | impersonate the     |                |                 |
|                   | context of          |                |                 |
|                   | DCS/PLC to gain     |                |                 |
|                   | additional          |                |                 |
|                   | privileges          |                |                 |
+-------------------+---------------------+----------------+-----------------+
| Threat_6.2.1_151, | Store & Forward     | Elevation of   | Avoid using     |
| Threat_6.2.1_152, | process may be      | privileges     | remote code     |
| Threat_6.2.1_166  | subject to          |                | execution       |
|                   | Elevation of        |                |                 |
|                   | Privilege using     |                |                 |
|                   | Remote Code         |                |                 |
|                   | Execution           |                |                 |
+-------------------+---------------------+----------------+-----------------+
| Threat_6.2.1_155, | Spoofing of         | Spoofing       | CIP should      |
| Threat_6.2.1_173  | external            |                | include         |
|                   | entities which      |                | standard        |
|                   | communicate or      |                | authentication  |
|                   | send/receive        |                | mechanism for   |
|                   | data to CIP         |                | all external    |
|                   |                     |                | entities        |
+-------------------+---------------------+----------------+-----------------+
| Threat_6.2.1_156  | External            | Repudiation    | Same mitigation |
|                   | entities            |                | as for          |
|                   | communicating       |                | T               |
|                   | with CIP            |                | hreat_6.2.1_146 |
|                   | potentially         |                |                 |
|                   | denies              |                |                 |
|                   | receiving data      |                |                 |
+-------------------+---------------------+----------------+-----------------+
| Threat_6.2.1_176  | Data flow           | Tempering      | CIP should      |
|                   | between             |                | include latest  |
|                   | authenticated       |                | encryption      |
|                   | external            |                | capabilities    |
|                   | entities            |                | and all data    |
|                   | tempered,           |                | should be       |
|                   | e.g. An             |                | shared in       |
|                   | attacker can        |                | encrypted form  |
|                   | read or modify      |                |                 |
|                   | data                |                |                 |
|                   | transmitted         |                |                 |
|                   | over an             |                |                 |
|                   | authenticated       |                |                 |
|                   | dataflow            |                |                 |
+-------------------+---------------------+----------------+-----------------+
| Threat_6.2.1_182  | Weak Access         | Authorization  | CIP should      |
|                   | Control for a       |                | provide         |
|                   | Resource            |                | guidelines to   |
|                   |                     |                | end product     |
|                   |                     |                | owners to have  |
|                   |                     |                | role based      |
|                   |                     |                | authorization   |
|                   |                     |                | policies for    |
|                   |                     |                | all users       |
+-------------------+---------------------+----------------+-----------------+
| Threat_6.2.1_191  | When external       | Authorization  | CIP should      |
|                   | entities can        | bypass         | provide         |
|                   | modify data         |                | guidelines all  |
|                   | without having      |                | external        |
|                   | privilege           |                | entities use    |
|                   |                     |                | the interfaces  |
|                   |                     |                | exposed for     |
|                   |                     |                | data            |
|                   |                     |                | modifications   |
+-------------------+---------------------+----------------+-----------------+
| Threat_6.2.1_195  | When CIP has        | Information    | CIP should keep |
|                   | weak Credential     | disclosure     | all credentials |
|                   | Storage or          |                | as encrypted    |
|                   | credentials are     |                | and same should |
|                   | stored without      |                | be recommended  |
|                   | encryption          |                | to end users    |
+-------------------+---------------------+----------------+-----------------+
| Threat_6.2.1_210  | An attacker         | Remote code    | CIP users       |
|                   | could execute       | execution      | should use      |
|                   | code remotely       |                | whitelisting to |
|                   | if he got           |                | ensure only     |
|                   | access to the       |                | known good      |
|                   | system.             |                | processes can   |
|                   |                     |                | be executed.    |
+-------------------+---------------------+----------------+-----------------+
| Threat_6.2.1_211  | An attacker         | System defense | CIP users       |
|                   | could               | weakend        | should enable   |
|                   | manipulate the      |                | secure time     |
|                   | system time to      |                | updates in      |
|                   | enable another      |                | their systems.  |
|                   | attack.             |                |                 |
+-------------------+---------------------+----------------+-----------------+

8. Validation of Threats and Mitigation 
---------------------------------------

TBD

9. CIP Core Packages for mitigation 
-----------------------------------

+-----------------------+-----------------------+-----------------------+
| Threat ID             | Mitigation            | Required package      |
+=======================+=======================+=======================+
| Threat_6.2.1_146      | CIP should address    | - auditd              |
| Threat_6.2.1_156      | this issue by         | - rsyslog             |
|                       | including logging or  |                       |
|                       | auditing to record    |                       |
|                       | the source, time, and |                       |
|                       | summary of the        |                       |
|                       | received data         |                       |
+-----------------------+-----------------------+-----------------------+
| Threat_6.2.1_148,     | Needs to discuss with | - TBD                 |
| Threat_6.2.1_163,     | members how to        |                       |
| Threat_6.2.1_164      | address this threat   |                       |
+-----------------------+-----------------------+-----------------------+
| Threat_6.2.1_150,     | Authentication and    | - shadow              |
|                       |                       | - pam                 |
|                       |                       | - openssl             |
|                       |                       | - google_authenticator|
| Threat_6.2.1_155,     | authorization of all  |                       |
| Threat_6.2.1_173      | external entities     |                       |
|                       |                       |                       |
+-----------------------+-----------------------+-----------------------+
| Threat_6.2.1_151,     | Avoid using remote    | - acl                 |
| Threat_6.2.1_152,     | code execution        | - Security policies   |
| Threat_6.2.1_166,     |                       |   based on end product|
| Threat_6.2.1_182,     |                       |                       |
| Threat_6.2.1_191      |                       |                       |
+-----------------------+-----------------------+-----------------------+
| Threat_6.2.1_176      | CIP should include    | - acl                 |
|                       | latest integrity      | - openssl             |
|                       | verification          |   Digital Signature   |
|                       | capabilities and all  |   verification        |
|                       | data should be shared | - sha256,sha512       |
|                       | in encryped form      |                       |
|                       |                       |                       |
+-----------------------+-----------------------+-----------------------+
| Threat_6.2.1_191      | CIP should provide    | - Document in APP &   |
|                       | guidelines all        |   HW guidelines       |
|                       | external entities use |                       |
|                       | the interfaces        |                       |
|                       | exposed for data      |                       |
|                       | modifications         |                       |
+-----------------------+-----------------------+-----------------------+
| Threat_6.2.1_195      | CIP should keep all   | - openssl             |
|                       | credentials as        | - acl                 |
|                       | encrypted and same    | - tpm2-tools          |
|                       | should be recommended | - tmp2-abrmd          |
|                       | to end users          | - tpm2-tss            |
+-----------------------+-----------------------+-----------------------+
| Threat_6.2.1_210      | CIP users should use  | - aide                |
|                       | aide for whitelisting |                       |
|                       | to ensure only known  |                       |
|                       | good processes can be |                       |
|                       | executed.             |                       |
+-----------------------+-----------------------+-----------------------+
| Threat_6.2.1_211      | CIP users should use  | - chrony              |
|                       | chrony to securely    |                       |
|                       | update their system   |                       |
|                       | time.                 |                       |
+-----------------------+-----------------------+-----------------------+

10. CIP Kernel Threat Modeling 
------------------------------

CIP Security WG needs to discuss with CIP Kernel WG how to approach
Threat Modeling for CIP Kernel, some of the options could be. 1.
Identify the risks for CIP Kernel 2. Identify important data flows which
might be compromised 3. Identify other sources which may pose risk to
CIP and mitigate

11. Updating CIP Threat Model 
-----------------------------

CIP Threat Model should be updated and revised based on following
conditions.

1. When CIP Core adapts new version of Debian
2. New Packages or functionality added which may be exploited by
   internal or external entities
3. When CIP adapts new version of CIP Kernel

12. Further Guidelines for End Product owners 
---------------------------------------------

End products owners are advised to follow steps listed here to re-use
existing CIP reference Threat Model.

-  Identify Security Requirements specific to the product
-  Identify business goals for the product
-  List down critical data flow for business scenarios
-  Use one the Threat Modeling methods for additional use cases

13. Acronyms 
------------

======= ========================================================
Acronym Detail
======= ========================================================
CIP     Civil Infrastructure Platform
CVE     Common Vulnerabilities and Exposures
CVSS    Common Vulnerability Scoring System
PLC     Programmable Logic Controller
DCS     Distributed Control System
STRIDE  Spoofing, Tampering, Repudiation, Information Disclosure
======= ========================================================

14. CIP Core CVE scanner 
------------------------

CIP Core uses CVSS threat modeling methodologies and uses Debian based
wrapper to automatically scan upstream repos and include the fixes.

The CVE scanner tool is available at

`CIP Core CVE
Scanner <https://gitlab.com/cip-playground/cip-core-sec>`__

15. CIP Kernel CVE scanner 
--------------------------

CIP Kernel uses CVE scanner to get latest CVE fixes and applies to CIP
Kernel. The repo is available at `CIP Kernel CVE
Scanner <https://gitlab.com/cip-project/cip-kernel/cip-kernel-sec>`__

16. References 
--------------

+---------------------------+----------------------------------------------------------------+
| Reference name            | Link                                                           |
+===========================+================================================================+
| Microsoft Threat Modeling | https://www.microsoft.com/en-in/download/details.aspx?id=49168 |
| Tool                      |                                                                |
+---------------------------+----------------------------------------------------------------+


17. Pending Work and known issues 
---------------------------------

1. Add setting for text wrapping, currently whole page is occupied by
   text
2. Split this document and keep CIP Kernel, CIP Core threat modeling in
   a separate document based on other members inputs
3. Discuss with other CIP WG members to identify real business CIP use
   case scenarios and create Threat Models for them
4. Further investigation to address embedded system specific exploits
   such as listed in
   https://www.apriorit.com/dev-blog/690-embedded-systems-attacks
