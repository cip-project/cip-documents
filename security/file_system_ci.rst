CIP Security Partitions
=======================

.. contents::

.. list-table:: Revision History
   :header-rows: 1
                              
   * - Revision No 
     - Date
     - Change description 
     - Author  
     - Reviewed by

   * - 001
     - 2021-11-29 
     - first draft  
     - Raphael Lisicki
     - To be reviewed by CIP Security WG members

1. Objective 
------------

The primary objective of this document is to explain how the integrity
and confidentiality of files stored on a device is maintained.

2. Partitions 
-------------

The following partition scheme is meant to illustrate the security
capabilities. Details regarding software update and hardware
architecture remain in the realm of the respective working groups. A x86
system using the A/B partition scheme for updates is assumed for this
illustration. The shown solutions provides integrity for the system
partition and confidentiality for user data.

.. figure:: ../resources/images/other_documents/partitions-security.jpg
   :alt: Partitions
