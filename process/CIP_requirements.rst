CIP Requirements
================

.. contents::

.. list-table:: Revision History
   :header-rows: 1
                              
   * - Revision No 
     - Date
     - Change description 
     - Author  
     - Reviewed by

   * - 001 
     - 2023-01-04
     - Template document for CIP requirements 
     - Sai Ashrith
     - Dinesh Kumar

   * - 002 
     - 2023-03-08
     - Add CIP functional and non-functional requirements   
     - Dinesh Kumar
     -  

   * - 003
     - 2023-07-10
     - Updated requirement IDs based on BV feedback.
     - Dinesh Kumar
     - TBR
     
   * - 004
     - 2023-11-10
     - Add process details while defining requirements
     - Sai Ashrith
     - TBR
     

Introduction 
------------

This document is intended to define and document CIP requirements as a
platform.There are generic CIP platform requirements which are mainly
derived from `CIP
white paper <https://www.cip-project.org/wp-content/uploads/sites/17/2018/10/CIP_Whitepaper_10.19.18.pdf>`__. 

IEC-62443-4-1 SM-1 expects the component to have defined requirements
which can be tested. The requirements can be functional, non-functional,
performance, security etc.

The basic goals of CIP have been documented in a whitepaper available at
`CIP project
portal <https://www.cip-project.org/wp-content/uploads/sites/17/2018/10/CIP_Whitepaper_10.19.18.pdf>`__.
According to the Certification Body the goals defined in the CIP
whitepaper are quite abstract and cannot be considered to meet
IEC-62443-4-1 Secure Development Process requirement.

Process of defining CIP Requirements 
------------------------------------

The process flow while defining CIP requirements is mentioned below:

1. When CIP is decided to be built as a platform, workgroups like TSC,
   CIP Core, CIP Kernel and security had numerous meetings to define a
   set of requirements which are platform based.
2. In every joint WG meeting, respective WG comes up with a proposal for
   a specific requirement.
3. All the WG members provide their opinion and vote whether to accept
   that proposal or reject it based on the scope and available
   expertise.
4. Finally, to build a platform on which numerous end-user applications
   can be developed, various features like **SLTS maintained kernel (RT
   & non-RT)**, **Multi-Architecture support**, **Security features
   based on IEC 62443-4-2 standard** are finalized and announced as CIP
   requirements.

CIP Functional Requirements 
---------------------------

+-----------------+-----------------+----------------------------------------------------------------------------------------------------------------+----------------+
| S No.           | Requirements    | Details                                                                                                        | Responsible WG |
+=================+=================+================================================================================================================+================+
| #               | Re-use Linux    | CIP to reuse                                                                                                   | CIP Kernel     |
| REQ-CIP-FUNC-01 | mainline        | Linux mainline                                                                                                 |                |
|                 | kernel,         | kernel                                                                                                         |                |
|                 | customise       |                                                                                                                |                |
|                 | configs based   |                                                                                                                |                |
|                 | on CIP members  |                                                                                                                |                |
|                 | requirement     |                                                                                                                |                |
+-----------------+-----------------+----------------------------------------------------------------------------------------------------------------+----------------+
| #               | Provide CIP RT  | CIP to maintain                                                                                                | CIP Kernel     |
| REQ-CIP-FUNC-02 | kernel by       | its own RT                                                                                                     |                |
|                 | applying        | kernel                                                                                                         |                |
|                 | PREEMPT_RT      |                                                                                                                |                |
|                 | patches         |                                                                                                                |                |
+-----------------+-----------------+----------------------------------------------------------------------------------------------------------------+----------------+
| #               | Develop         | Create recipes                                                                                                 | CIP Core       |
| REQ-CIP-FUNC-03 | meta-data to    | and meta-data                                                                                                  |                |
|                 | create minimal  | to re-use                                                                                                      |                |
|                 | CIP reference   | Debian packages                                                                                                |                |
|                 | images          | for creating                                                                                                   |                |
|                 |                 | minimal CIP                                                                                                    |                |
|                 |                 | reference image                                                                                                |                |
+-----------------+-----------------+----------------------------------------------------------------------------------------------------------------+----------------+
| #               | Support         | Recipes and                                                                                                    | CIP Core, CIP  |
| REQ-CIP-FUNC-04 | multiple cpu    | meta-data                                                                                                      | Kernel         |
|                 | architectures   | should be                                                                                                      |                |
|                 | in CIP          | configurable to                                                                                                |                |
|                 | reference       | support                                                                                                        |                |
|                 | images          | multiple                                                                                                       |                |
|                 |                 | architectures                                                                                                  |                |
|                 |                 | such as amd64,                                                                                                 |                |
|                 |                 | arm64, armhf                                                                                                   |                |
+-----------------+-----------------+----------------------------------------------------------------------------------------------------------------+----------------+
| #               | Support Secure  | Support secure                                                                                                 | CIP Core, CIP  |
| REQ-CIP-FUNC-05 | boot            | boot with or                                                                                                   | Kernel         |
|                 |                 | without secure                                                                                                 |                |
|                 |                 | storage                                                                                                        |                |
+-----------------+-----------------+----------------------------------------------------------------------------------------------------------------+----------------+
| #               | Support         | CIP users                                                                                                      | CIP SWUpdate   |
| REQ-CIP-FUNC-06 | SWUpdate with   | should be able                                                                                                 |                |
|                 | local file and  | to update                                                                                                      |                |
|                 | OTA             | devices using                                                                                                  |                |
|                 |                 | local file                                                                                                     |                |
|                 |                 | using sdcard or                                                                                                |                |
|                 |                 | eMMC or using                                                                                                  |                |
|                 |                 | OTA updates                                                                                                    |                |
+-----------------+-----------------+----------------------------------------------------------------------------------------------------------------+----------------+
| #               | Support         | CIP should                                                                                                     | CIP SWUpdate   |
| REQ-CIP-FUNC-07 | SWUpdate with   | support                                                                                                        |                |
|                 | signed &        | SWUpdate with                                                                                                  |                |
|                 | encrypted       | Signed and                                                                                                     |                |
|                 | images          | Encrypted                                                                                                      |                |
|                 |                 | images                                                                                                         |                |
+-----------------+-----------------+----------------------------------------------------------------------------------------------------------------+----------------+
| #               | CIP Security    | `CIP Security                                                                                                  | CIP SWG & CIP  |
| REQ-CIP-FUNC-08 | detailed        | Requirements <https://gitlab.com/cip-project/cip-documents/-/blob/master/security/security_requirements.rst>`__| Core           |
|                 | requirements    |                                                                                                                |                |
|                 | are documented  |                                                                                                                |                |
|                 | in a separate   |                                                                                                                |                |
|                 | document at     |                                                                                                                |                |
+-----------------+-----------------+----------------------------------------------------------------------------------------------------------------+----------------+
| #               | Deliver a       | The CIP                                                                                                        | CIP Core, CIP  |
| REQ-CIP-FUNC-09 | generatable     | packages, the                                                                                                  | Kernel, CIP    |
|                 | SBOM along with | tooling to                                                                                                     | SWUpdate       |
|                 | the sample      | create the                                                                                                     |                |
|                 | configuration   | packages and                                                                                                   |                |
|                 |                 | system image                                                                                                   |                |
|                 |                 | for the                                                                                                        |                |
|                 |                 | reference                                                                                                      |                |
|                 |                 | hardware shall                                                                                                 |                |
|                 |                 | be enabled to                                                                                                  |                |
|                 |                 | also provide a                                                                                                 |                |
|                 |                 | SBOM for the                                                                                                   |                |
|                 |                 | provided                                                                                                       |                |
|                 |                 | software                                                                                                       |                |
+-----------------+-----------------+----------------------------------------------------------------------------------------------------------------+----------------+


CIP Non-Functional Requirements 
-------------------------------

+-----------------+-----------------+-----------------+-----------------+
| S No.           | Requirements    | Details         | Responsible WG  |
+=================+=================+=================+=================+
| #REQ-           | Follow upstream | CIP members to  | CIP Kernel      |
| CIP-NON-FUNC-01 | first policy    | follow upstream |                 |
|                 | for CIP Core    | policy for the  |                 |
|                 | and CIP Kernel  | issue fixes in  |                 |
|                 | development     | CIP Kernel or   |                 |
|                 |                 | CIP Core should |                 |
|                 |                 | be first        |                 |
|                 |                 | upstreamed      |                 |
|                 |                 | before          |                 |
|                 |                 | accepting in    |                 |
|                 |                 | CIP             |                 |
+-----------------+-----------------+-----------------+-----------------+
| #REQ-           | Maintain SLTS   | CIP members to  | CIP Kernel      |
| CIP-NON-FUNC-02 | kernel for 10+  | decide          |                 |
|                 | years           | democratically  |                 |
|                 |                 | SLTS kernel and |                 |
|                 |                 | maintain for up |                 |
|                 |                 | to 10 years by  |                 |
|                 |                 | providing       |                 |
|                 |                 | security fixes  |                 |
|                 |                 | and updates to  |                 |
|                 |                 | CIP users       |                 |
+-----------------+-----------------+-----------------+-----------------+
| #REQ-           | Use Debian      | The primary     | CIP Core        |
| CIP-NON-FUNC-03 | based packages  | source of CIP   |                 |
|                 | or third party  | Core packages   |                 |
|                 | applications to | is Debian       |                 |
|                 | create CIP Core | repositories.   |                 |
|                 | reference       | However, some   |                 |
|                 | images          | packages may    |                 |
|                 |                 | also come from  |                 |
|                 |                 | other           |                 |
|                 |                 | repositories    |                 |
|                 |                 | based on all    |                 |
|                 |                 | members         |                 |
|                 |                 | decision        |                 |
+-----------------+-----------------+-----------------+-----------------+
| #REQ-           | Accept only     | CIP Kernel      | CIP Kernel      |
| CIP-NON-FUNC-04 | kernel patches  | maintainers to  |                 |
|                 | which are       | ensure all the  |                 |
|                 | upstreamed      | patches applied |                 |
|                 |                 | in the CIP      |                 |
|                 |                 | kernel are from |                 |
|                 |                 | stable upstream |                 |
|                 |                 | trees           |                 |
+-----------------+-----------------+-----------------+-----------------+

CIP Security Requirements 
-------------------------

As CIP did not have any clearly defined security requirements hence CIP
Security requirements have been taken from IEC-62443-4-2 in order to add
security capabilities.

CIP Security requirements are documented at `CIP Security
Requirements <https://gitlab.com/cip-project/cip-documents/-/blob/master/security/security_requirements.rst>`__
