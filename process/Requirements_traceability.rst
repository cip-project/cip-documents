Traceability from CIP requirements to design and testing
========================================================

.. contents::

.. list-table:: Revision History
   :header-rows: 1
                              
   * - Revision No 
     - Date
     - Change description 
     - Author  
     - Reviewed by

   * - 001
     - 2023-09-15
     - Draft document for traceability from CIP requirements to design and testing process
     - Sai Ashrith 
     - TBR

   * - 002  
     - 2023-10-10
     - Updated design and test descriptions by adding references to user manual document
     - Sai Ashrith  
     - TBR

   * - 003
     - 2023-11-09
     - Added details regarding process involved in making the traceability matrices   
     - Sai Ashrith  
     - TBR

Introduction 
~~~~~~~~~~~~

This document shows the traceability from CIP functional and
non-functional requirements to the respective design and testing process
followed by the CIP WG members to fulfill those requirements.

Acronyms 
~~~~~~~~

===== ======= =========================================
S No. Acronym Definition
===== ======= =========================================
1     WG      Workgroup
2     CIP     Civil Infrastructure Platform
3     SLTS    Super Long Term Support
4     OTA     Over The Air
5     SWG     Security Work Group
6     IEC     International Electrotechnical Commission
7     SBOM    Software Bill Of Materials
8     RT      Real-Time
9     CI      Continous Integration
===== ======= =========================================

Process behind creating requirement traceability matrices 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The process flow to document the below traceability matrics is mentioned
below:

1. CIP SWG members documented the functional and non-functional
   requirements in `here <https://gitlab.com/cip-project/cip-documents/-/blob/master/process/CIP_requirements.rst?ref_type=heads>`__ which is used while
   creating this traceability document.
2. While mapping the respective design and testing evidence to the
   finalized requirements, CIP SWG members had thorough discussions with
   CIP-Core WG and CIP Testing WG members to obtain respective evidence
   for the defined requirements.

The unfilled traceability data shall be documented after further
discussion with respective WG members.

Traceability matrix from CIP requirements to design 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

+----------------------+----------------------------------------+---------------------------------------+
| Req ID               | Requirement                            | CIP Design                            | 
|                      |                                        | description                           |
+======================+========================================+=======================================+
| #REQ-CIP-FUNC-01     | Reuse Linux mainline kernel, customise | CIP Kernel WG                         |
|                      | configs based on CIP members           | members reuse                         |
|                      | requirement                            | mainline kernel                       |
|                      |                                        | and maintain it                       |
|                      |                                        | for                                   |
|                      |                                        | `SLTS                                 |
|                      |                                        | <https://git.kerne                    |
|                      |                                        | l.org/pub/scm/linu                    |
|                      |                                        | x/kernel/git/cip/l                    |
|                      |                                        | inux-cip.git/>`__ The kernel          |
|                      |                                        | configurations                        |
|                      |                                        | required for CIP                      |
|                      |                                        | use cases are made                    |
|                      |                                        | and maintained in                     |
|                      |                                        | this                                  |
|                      |                                        | `repository <https://g                |
|                      |                                        | itlab.com/cip-proj                    |
|                      |                                        | ect/cip-kernel/cip                    |
|                      |                                        | -kernel-config>`__                    |
|                      |                                        | for all supported                     |
|                      |                                        | versions.                             |
+----------------------+----------------------------------------+---------------------------------------+
| #REQ-CIP-FUNC-02     | Provide CIP RT kernel by applying      | CIP Kernel WG                         |
|                      | PREEMPT_RT patches                     | members also                          |
|                      |                                        | maintain the                          |
|                      |                                        | real-time versions                    |
|                      |                                        | of the Long term                      |
|                      |                                        | supported kernel                      |
|                      |                                        | branches for 10+                      |
|                      |                                        | life period.                          |
|                      |                                        | Additional details                    |
|                      |                                        | regarding kernel                      |
|                      |                                        | maintenance are                       |
|                      |                                        | available in the                      |
|                      |                                        | `CIP user manual  <https://gitl       |
|                      |                                        | ab.com/cip-project                    |
|                      |                                        | /cip-documents/-/b                    |
|                      |                                        | lob/master/user/us                    |
|                      |                                        | er_manual/user_man                    |
|                      |                                        | ual.rst?ref_type=he                   |
|                      |                                        | ads#cip-kernel>`__ document.          |
+----------------------+----------------------------------------+---------------------------------------+
| #REQ-CIP-FUNC-03     | Develop meta-data to create minimal    | CIP developers                        |
|                      | CIP reference images                   | consistently                          |
|                      |                                        | develop recipes to                    |
|                      |                                        | create CIP                            |
|                      |                                        | reference images                      |
|                      |                                        | for various                           |
|                      |                                        | architectures,                        |
|                      |                                        | Debian suites with                    |
|                      |                                        | supported Kernel                      |
|                      |                                        | versions along                        |
|                      |                                        | with additional                       |
|                      |                                        | features like                         |
|                      |                                        | software update,                      |
|                      |                                        | secure boot,                          |
|                      |                                        | security layer,                       |
|                      |                                        | data encrpytion                       |
|                      |                                        | etc. This                             |
|                      |                                        | meta-data is                          |
|                      |                                        | maintained in                         |
|                      |                                        | `isar-cip-core <htt                   |
|                      |                                        | ps://gitlab.com/ci                    |
|                      |                                        | p-project/cip-core                    |
|                      |                                        | /isar-cip-core>`__ repository         |
+----------------------+----------------------------------------+---------------------------------------+
| #REQ-CIP-FUNC-04     | Support multiple cpu architectures in  | CIP currently                         |
|                      | CIP reference images                   | supports `mentioned <https:           |
|                      |                                        | //gitlab.com/cip-p                    |
|                      |                                        | roject/cip-core/is                    |
|                      |                                        | ar-cip-core/-/tree                    |
|                      |                                        | /master/kas/board?                    |
|                      |                                        | ref_type=heads>`__ architectures with |
|                      |                                        | their meta-data in                    |
|                      |                                        | their reference                       |
|                      |                                        | images.                               |
+----------------------+----------------------------------------+---------------------------------------+
| #REQ-CIP-FUNC-05     | Support Secure boot                    | CIP implemented                       |
|                      |                                        | recipes to enable                     |
|                      |                                        | secure boot on                        |
|                      |                                        | their supported                       |
|                      |                                        | architectures.                        |
|                      |                                        | Additional details                    |
|                      |                                        | regarding Secure                      |
|                      |                                        | boot design in CIP                    |
|                      |                                        | is available in                       |
|                      |                                        | the `user                             |
|                      |                                        | manual <htt                           |
|                      |                                        | ps://gitlab.com/ci                    |
|                      |                                        | p-project/cip-docu                    |
|                      |                                        | ments/-/blob/master                   |
|                      |                                        | /user/user_manual                     |
|                      |                                        | /user_manual.rst?re                   |
|                      |                                        | f_type=heads#secur                    |
|                      |                                        | e-boot-feature>`__                    |
|                      |                                        | document.                             |
|                      |                                        |                                       |
+----------------------+----------------------------------------+---------------------------------------+
| #REQ-CIP-FUNC-06     | Support SWUpdate with local file and   | CIP SWupdate WG                       |
|                      | OTA                                    | designed and                          |
|                      |                                        | developed recipes                     |
|                      |                                        | to enable                             |
|                      |                                        | local,o                               |
|                      |                                        | ver-the-air,signed                    |
|                      |                                        | and unsigned                          |
|                      |                                        | SWupdate. Swupdate                    |
|                      |                                        | package from                          |
|                      |                                        | Debian is used in                     |
|                      |                                        | their design to                       |
|                      |                                        | achieve this.                         |
|                      |                                        | Additional details                    |
|                      |                                        | about types of                        |
|                      |                                        | software updates                      |
|                      |                                        | available in CIP                      |
|                      |                                        | are mentioned in                      |
|                      |                                        | the `user                             |
|                      |                                        | manual <http                          |
|                      |                                        | s://gitlab.com/cip                    |
|                      |                                        | -project/cip-docum                    |
|                      |                                        | ents/-/blob/master                    |
|                      |                                        | /user/user_manual/                    |
|                      |                                        | user_manual.rst?ref                   |
|                      |                                        | _type=heads#cip-so                    |
|                      |                                        | ftware-updates>`__                    |
|                      |                                        | document                              |
+----------------------+----------------------------------------+---------------------------------------+
| #REQ-CIP-FUNC-07     | Support SWUpdate with signed &         | CIP WG designed                       |
|                      | encrypted images                       | and developed                         |
|                      |                                        | recipes to create                     |
|                      |                                        | encrypted                             |
|                      |                                        | reference images                      |
|                      |                                        | which are also                        |
|                      |                                        | simultaneously                        |
|                      |                                        | signed and can                        |
|                      |                                        | also be updated.                      |
+----------------------+----------------------------------------+---------------------------------------+
| #REQ-CIP-FUNC-08     | Support security features based on IEC | CIP SWG added                         |
|                      | 624443-4-2 standard                    | security related                      |
|                      |                                        | Debian packages in                    |
|                      |                                        | their design to                       |
|                      |                                        | fulfill security                      |
|                      |                                        | requirements based                    |
|                      |                                        | on IEC 62443-4-2.                     |
|                      |                                        | The list of                           |
|                      |                                        | security related                      |
|                      |                                        | packages which are                    |
|                      |                                        | added to harden                       |
|                      |                                        | the CIP IEC layer                     |
|                      |                                        | are mentioned in                      |
|                      |                                        | this `security                        |
|                      |                                        | hardening <htt                        |
|                      |                                        | ps://gitlab.com/ci                    |
|                      |                                        | p-project/cip-docu                    |
|                      |                                        | ments/-/blob/maste                    |
|                      |                                        | r/security/CIP_Sec                    |
|                      |                                        | urity_Hardening.rst                   |
|                      |                                        | ?ref_type=heads#4-                    |
|                      |                                        | technical-implemen                    |
|                      |                                        | tation-details>`__                    |
|                      |                                        | document.                             |
+----------------------+----------------------------------------+---------------------------------------+
| #REQ-CIP-FUNC-09     | Deliver a generatable SBOM along with  | TODO: Need to                         |
|                      | the sample configuration               | discuss with CIP                      |
|                      |                                        | members                               |
+----------------------+----------------------------------------+---------------------------------------+
| #REQ-CIP-NON-FUNC-01 | Follow upstream first policy for CIP   | TODO: Shall be                        |
|                      | Core and CIP Kernel development        | updated in future                     |
+----------------------+----------------------------------------+---------------------------------------+
| #REQ-CIP-NON-FUNC-02 | Maintain SLTS kernel for 10+ years     | TODO: Shall be                        |
|                      |                                        | updated in future                     |
+----------------------+----------------------------------------+---------------------------------------+
| #REQ-CIP-NON-FUNC-03 | Use Debian based packages or third     | CIP uses latest                       |
|                      | party applications to create CIP Core  | stable Debian                         |
|                      | reference images                       | packages according                    |
|                      |                                        | to their use cases                    |
|                      |                                        | to build the base                     |
|                      |                                        | system for the                        |
|                      |                                        | reference images.                     |
+----------------------+----------------------------------------+---------------------------------------+
| #REQ-CIP-NON-FUNC-04 | Accept only kernel patches which are   | The patches are                       |
|                      | upstreamed                             | applied to CIP                        |
|                      |                                        | Kernel under the                      |
|                      |                                        | criteria that they                    |
|                      |                                        | are accepted first                    |
|                      |                                        | in upstream stable                    |
|                      |                                        | branches                              |
|                      |                                        | maintained by Greg                    |
|                      |                                        | Kroah Hartmann                        |
+----------------------+----------------------------------------+---------------------------------------+

Traceability matrix from CIP requirements to testing process 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

+----------------------+-------------------------------------------+---------------------------------------------+
| Req ID               | Requirement                               | CIP testing                                 | 
|                      |                                           | description                                 |
+======================+===========================================+=============================================+
| #REQ-CIP-FUNC-01     | Reuse Linux mainline kernel, customise    | Automated                                   |
|                      | configs based on CIP members requirement  | tests run in                                |
|                      |                                           | `LAVA Lab <https://lava                     |
|                      |                                           | .ciplatform.or                              |
|                      |                                           | g/results/>`__                              |
|                      |                                           | and `Kernel CI <https://lin                 |
|                      |                                           | ux.kernelci.org/job/cip/>`__                |
|                      |                                           | whenever                                    |
|                      |                                           | changes are                                 |
|                      |                                           | made to CIP                                 |
|                      |                                           | Kernel.                                     |
+----------------------+-------------------------------------------+---------------------------------------------+
| #REQ-CIP-FUNC-02     | Provide CIP RT kernel by applying         | Real-time                                   |
|                      | PREEMPT_RT patches                        | kernels are                                 |
|                      |                                           | also tested                                 |
|                      |                                           | using                                       |
|                      |                                           | automated test                              |
|                      |                                           | jobs                                        |
|                      |                                           | implemented in                              |
|                      |                                           | LAVA lab and                                |
|                      |                                           | Kernel CI.                                  |
+----------------------+-------------------------------------------+---------------------------------------------+
| #REQ-CIP-FUNC-03     | Develop meta-data to create minimal CIP   | TODO: Discuss                               |
|                      | reference images                          | among CIP-Core                              |
|                      |                                           | WG members and                              |
|                      |                                           | finalize                                    |
|                      |                                           | meta-data                                   |
|                      |                                           | testing method                              |
+----------------------+-------------------------------------------+---------------------------------------------+
| #REQ-CIP-FUNC-04     | Support multiple cpu architectures in CIP | Whenever the                                |
|                      | reference images                          | metadata is                                 |
|                      |                                           | modified, a CI                              |
|                      |                                           | runs to test                                |
|                      |                                           | the builds of                               |
|                      |                                           | reference                                   |
|                      |                                           | images on all                               |
|                      |                                           | supported                                   |
|                      |                                           | architectures.                              |
|                      |                                           | Here is a                                   |
|                      |                                           | `sample result <https://g                   |
|                      |                                           | itlab.com/cip-project/cip-co                |
|                      |                                           | re/isar-cip-core/-/pipelines                |
|                      |                                           | /985520055>`__ which shows                  |
|                      |                                           | the build test                              |
|                      |                                           | results on all                              |
|                      |                                           | supported                                   |
|                      |                                           | architectures.                              |
+----------------------+-------------------------------------------+---------------------------------------------+
| #REQ-CIP-FUNC-05     | Support Secure boot                       | Currently an                                |
|                      |                                           | `automated test case <h                     |
|                      |                                           | ttps://gitlab.com/cip-projec                |
|                      |                                           | t/cip-testing/cip-security-t                |
|                      |                                           | ests/-/merge_requests/13>`__ is implemented |
|                      |                                           | in CIP IEC                                  |
|                      |                                           | layer which                                 |
|                      |                                           | verifies                                    |
|                      |                                           | whether Secure                              |
|                      |                                           | boot is                                     |
|                      |                                           | enabled or not                              |
|                      |                                           | for amd64,                                  |
|                      |                                           | arm64 and                                   |
|                      |                                           | armhf                                       |
|                      |                                           | architectures.                              |
+----------------------+-------------------------------------------+---------------------------------------------+
| #REQ-CIP-FUNC-06     | Support SWUpdate with local file and OTA  | The process to                              |
|                      |                                           | test SWupdate                               |
|                      |                                           | on CIP                                      |
|                      |                                           | reference                                   |
|                      |                                           | images is                                   |
|                      |                                           | clearly                                     |
|                      |                                           | documented in                               |
|                      |                                           | this                                        |
|                      |                                           | `manual <https://gitl                       |
|                      |                                           | ab.com/cip-pro                              |
|                      |                                           | ject/cip-core/                              |
|                      |                                           | isar-cip-core/                              |
|                      |                                           | -/blob/master/                              |
|                      |                                           | doc/README.swu                              |
|                      |                                           | pdate.md?ref_t                              |
|                      |                                           | ype=heads>`__.                              |
+----------------------+-------------------------------------------+---------------------------------------------+
| #REQ-CIP-FUNC-07     | Support SWUpdate with signed & encrypted  | CIP-Core                                    |
|                      | images                                    | meta-data                                   |
|                      |                                           | allows to                                   |
|                      |                                           | build                                       |
|                      |                                           | en                                          |
|                      |                                           | crypted-signed                              |
|                      |                                           | reference                                   |
|                      |                                           | images. So the                              |
|                      |                                           | steps                                       |
|                      |                                           | mentioned in                                |
|                      |                                           | this                                        |
|                      |                                           | `testing <https://gi                        |
|                      |                                           | tlab.com/cip-p                              |
|                      |                                           | roject/cip-cor                              |
|                      |                                           | e/isar-cip-cor                              |
|                      |                                           | e/-/blob/maste                              |
|                      |                                           | r/doc/README.s                              |
|                      |                                           | wupdate.md>`__                              |
|                      |                                           | document can                                |
|                      |                                           | be used to                                  |
|                      |                                           | test the                                    |
|                      |                                           | SWupdate                                    |
|                      |                                           | functionality.                              |
+----------------------+-------------------------------------------+---------------------------------------------+
| #REQ-CIP-FUNC-08     | Support security features based on IEC    | CIP SWG                                     |
|                      | 624443-4-2 standard                       | developed test                              |
|                      |                                           | scripts to                                  |
|                      |                                           | test the                                    |
|                      |                                           | security                                    |
|                      |                                           | features of                                 |
|                      |                                           | the Debian                                  |
|                      |                                           | packages                                    |
|                      |                                           | installed the                               |
|                      |                                           | security                                    |
|                      |                                           | image. The                                  |
|                      |                                           | steps to run                                |
|                      |                                           | these tests                                 |
|                      |                                           | are clearly                                 |
|                      |                                           | documented                                  |
|                      |                                           | `here  <http                                |
|                      |                                           | s://gitlab.com                              |
|                      |                                           | /cip-project/c                              |
|                      |                                           | ip-core/isar-c                              |
|                      |                                           | ip-core/-/blob                              |
|                      |                                           | /master/doc/RE                              |
|                      |                                           | ADME.security-                              |
|                      |                                           | testing.md>`__                              |
+----------------------+-------------------------------------------+---------------------------------------------+
| #REQ-CIP-FUNC-09     | Deliver a generatable SBOM along with the | TODO: Shall be                              |
|                      | sample configuration                      | updated in                                  |
|                      |                                           | future after                                |
|                      |                                           | discussion                                  |
|                      |                                           | with CIP                                    |
|                      |                                           | members                                     |
+----------------------+-------------------------------------------+---------------------------------------------+
| #REQ-CIP-NON-FUNC-01 | Follow upstream first policy for CIP Core | TODO: Shall be                              |
|                      | and CIP Kernel development                | updated in                                  |
|                      |                                           | future                                      |
+----------------------+-------------------------------------------+---------------------------------------------+
| #REQ-CIP-NON-FUNC-02 | Maintain SLTS kernel for 10+ years        | TODO: Shall be                              |
|                      |                                           | updated in                                  |
|                      |                                           | future                                      |
+----------------------+-------------------------------------------+---------------------------------------------+
| #REQ-CIP-NON-FUNC-03 | Use Debian based packages or third party  | CIP uses                                    |
|                      | applications to create CIP Core reference | latest stable                               |
|                      | images                                    | Debian                                      |
|                      |                                           | packages                                    |
|                      |                                           | according to                                |
|                      |                                           | their use                                   |
|                      |                                           | cases to build                              |
|                      |                                           | the base                                    |
|                      |                                           | system for the                              |
|                      |                                           | reference                                   |
|                      |                                           | images.                                     |
+----------------------+-------------------------------------------+---------------------------------------------+
| #REQ-CIP-NON-FUNC-04 | Accept only kernel patches which are      | TODO: Shall be                              |
|                      | upstreamed                                | updated in                                  |
|                      |                                           | future                                      |
+----------------------+-------------------------------------------+---------------------------------------------+
