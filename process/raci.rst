Roles and Responsibilities
==========================

.. contents::

.. list-table:: Revision History
   :header-rows: 1
                              
   * - Revision No 
     - Date
     - Change description 
     - Author  
     - Reviewed by

   * - 001 
     - 2021-08-26
     - Draft RACI document in CIP
     - Yasin Demirci
     - To be reviewed by CIP Security WG members

   * - 002 
     - 2022-01-06 
     - Changed to better reflect SM-2
     - Yasin User
     - To be reviewed by CIP Security WG members

   * - 003
     - 2024-01-11
     - Updated based on BV feedback to add more details
     - Dinesh Kumar
     - TBR
     

1. Objective 
------------

The primary objective of this document is to show the roles in CIP with
their responsibilities and accountabilities. It is also shown which
roles should be consulted and/or informed for certain actions and which
qualifications, if any, are needed to fulfill a role.

2. Scope 
--------

Scope of this document is to meet IEC-62443-4-1 SM-2 (Identification of
Responsibilities) security requirement.

4. Roles 
--------

+--------------+-------------+---------------------+---------------------+
| Abbreviation | Name        | Description         | Qualifications      |
+==============+=============+=====================+=====================+
| SWG          | Security    | The SWG handles all | >50% of the SWG     |
|              | Working     | IT security topics  | members need to     |
|              | Group       | for CIP. This       | have at least 3     |
|              |             | includes consulting | years of experience |
|              |             | other working       | in IT security or   |
|              |             | groups and adding   | proof their         |
|              |             | additional security | expertise via       |
|              |             | features. All       | certifications.     |
|              |             | decisions are made  |                     |
|              |             | via the security    |                     |
|              |             | mailing list or     |                     |
|              |             | meetings of the     |                     |
|              |             | SWG.                |                     |
+--------------+-------------+---------------------+---------------------+
| MNT          | CIP         | CIP maintainers are | All CIP maintainers |
|              | Maintainers | usually members of  | have to show        |
|              |             | the Kernel or Core  | evidence for at     |
|              |             | working groups.     | least 3 days of     |
|              |             |                     | secure coding       |
|              |             |                     | training.           |
+--------------+-------------+---------------------+---------------------+
| TSC          | Technical   | The technical       | TSC members do not  |
|              | Steering    | steering committee  | need security       |
|              | Committee   | consists            | qualifications as   |
|              |             | representatives of  | they are consulted  |
|              |             | the member          | by the security     |
|              |             | companies. They     | working group.      |
|              |             | vote on changes     |                     |
|              |             | suggested by the    |                     |
|              |             | working groups.     |                     |
+--------------+-------------+---------------------+---------------------+
| TST          | CIP Tester  | CIP maintainers are | All CIP testers     |
|              |             | usually members of  | have to show        |
|              |             | the Kernel, Core or | evidence for at     |
|              |             | Testing working     | least 3 days of     |
|              |             | groups.             | secure coding       |
|              |             |                     | training or a       |
|              |             |                     | similar training    |
|              |             |                     | for secure testing. |
+--------------+-------------+---------------------+---------------------+

5. RACI 
-------

===================================== === === === ===
\                                     SWG MNT TSC TST
===================================== === === === ===
Update Secure Coding Standard         a r c   i   i
Provide File Integrity                a r i   i   -
Controls for Private Keys             a   r   i   r
Product Security Context              a r -   i   -
Threat Model                          a r -   i   -
Product Security Requirements         a r c   i   -
Product Security Requirements Content a r -   i   -
Security Requirements Review          a r -   i   -
Secure Coding Standards               a r c   i   -
Security Update Qualification         a c r   i   r
Security Update Documentation         a c r   i   -
===================================== === === === ===

Legend: - a = accountable - r = responsible - c = consulted - i =
informed - - = not applicable

Note: Ultimately, The CIP governing board and the Linux Foundation are
accountable for the whole CIP project. The RACI matrix above instead
shows who is responsible and accountable from an everyday business
perspective.

6. Roles and Responsibilities
-----------------------------

+--------------------------------+----------------------------------------------------+-----------------------------------------------------+
| Roles                          | Responsibilities                                   | Related Examples/Members                            |
|                                |                                                    |                                                     |
+================================+====================================================+=====================================================+
| Kernel WG chair, Maintainer    | 1. Review and approve kernel patches and documents | 1. Kernel WG chair: Jan Kiszka                      |
|                                | 2. Maintain all CIP kernel and make releases       | 2. Kernel Maintainer: Pavel Machek,Iwamatsu Nobuhiro|
|                                | 3. Apply Security fixes from upstream              |                                                     |
+--------------------------------+----------------------------------------------------+-----------------------------------------------------+
| CIP Core WG chair, Maintainer  | 1. Review and approve CIP Core patches & documents | 1. CIP Core WG chair: Hayashi Kazuhiro              |
|                                | 2. Maintain isar-cip-core gitlab repo              | 2. CIP Core Maintainer: Jan Kiszka                  |
|                                | 3. Apply Security fixes from upstream, make        |                                                     |
|                                |    releases of meta-data                           |                                                     |
+--------------------------------+----------------------------------------------------+-----------------------------------------------------+
| Security WG chair, Maintainer  | 1. Review and approve CIP IEC-62443 artifacts      | 1. Security WG chair: **Dinesh Kumar (Acting)**     |
|                                |                                                    | 2. Maintainer: Stefan Shroeder, Dinesh Kumar        |
+--------------------------------+----------------------------------------------------+-----------------------------------------------------+
