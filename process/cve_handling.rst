CIP CVE handling
================

.. contents::

.. list-table:: Revision History
   :header-rows: 1
                              
   * - Revision No 
     - Date
     - Change description 
     - Author  
     - Reviewed by

   * - 001 
     - 2023-09-05
     - Draft CVE handling document in CIP  
     - Dinesh Kumar (SWG), Masami Ichikawa (Kernel WG)
     - TBR

   * - 002 
     - 2023-09-12
     - Incorporated feedback from CIP Core and SWG member's     
     - Dinesh Kumar 
     - Kazu-san (CIP Core), Stefan (SWG).

1. Objective 
------------

The primary objective of this document is to clarify how CVE handling is
done in CIP. CVE handling in isar-cip-core and CIP kernel is done in
slightly way.

2. Scope 
--------

Scope of this document is to meet IEC-62443-4-1 DM-1 to DM-5 (Receiving
notifications of security issues) security requirements.

3. CIP Core CVE Handling 
------------------------

As part of isar-cip-core all packages are reused from upstream projects
primarily from Debian repositories. While creating CIP reference images
no changes are introduced by CIP developers in Debian packages. So as
part of isar-cip-core CVE handling, only CVE details for installed
packages is collected and shared with CIP users.

The diagram in
`debian-cve-checker <https://gitlab.com/cip-playground/debian-cve-checker>`__
depicts how fetching CVEs from upstream CVE databases work.

Details of how to generate CVE reports for isar-cip-core is available in
the gitlab repository `Gitlab CIP
Playground <https://gitlab.com/cip-playground/cip-core-sec>`__

CIP Core WG members share CVE reports on regular basis with CIP users.

3.1 Security Analysis of CIP Core CVEs 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

CIP members rely on upstream for security analysis for any CVEs found or
reported in the related packages installed on CIP reference images. One
of the primary source is `Debian Security
Tracker <https://security-team.debian.org/security_tracker.html>`__. As
Debian Security team analyses multiple sources such as other Linux
distributions for security issues, CIP users can safely rely on the
information published by Debian Security information.

CIP users are recommended to do a risk assessment of the packages based
on the specific uses cases where CVEs are open and are having high
severity as sometimes even the severity of CVE might be high but it
might not be relevant for Debian system or may not impact specific use
cases. Additionally, as Debian DSA/DLA are published after deep
technical analysis of CVEs which impact Debian system, so CIP users
should also refer Debian DSA/DLA information.

`Debian Security
Tracker <https://security-team.debian.org/security_tracker.html>`__ and
`FAQ page <https://www.debian.org/security/faq>`__ is insightful source
of information for further reference.

The CVE report shared by CIP for isar-cip-core packages is for CIP
user’s reference. Based on the CVE report CIP users are advised to
update their systems to receive latest security updates.

``Overall the primary objective of CIP Core CVE checker tool is to provide CIP users an easy way to generate CVE reports on their systems based on installed package list.``

4. CIP Kernel CVE Handling 
--------------------------

CIP Kernel maintainers maintain CVE handling repository at `CIP Kernel
Sec <https://gitlab.com/cip-project/cip-kernel/cip-kernel-sec>`__.

Following diagram illustrates basic flow of generating CVE report for
CIP kernel. The generated report is shared with CIP Kernel users on
regular basis.

.. uml:: plantuml

   @startuml
   left to right direction
   database "ubuntu-cve-tracker" as D1
   database "debian-kernel-sec" as D2
   database "stable/backported\n git commit messages" as D3
   database "cip-kernel-sec Issue DB" as D4
   file "CIP Kernel\nCVE Reports" as f1
   D1-->D4 : scripts/\nimport_ubuntu.py 
   D2-->D4 : scripts/import_debian.py
   D3-->D4 : scripts/import_stable.py
   D4-->f1 : scripts/report_affected.py
   @enduml

The primary source of getting CVE information is `Debian’s Linux kernel
CVE tracking
repository <https://salsa.debian.org/kernel-team/kernel-sec>`__ and
`Ubuntu’s CVE tracking
repository <https://git.launchpad.net/ubuntu-cve-tracker/tree/>`__.

In addition, following CVE data sources are also checked to get more
details about CVEs.

-  https://cve.mitre.org/
-  https://nvd.nist.gov/
-  https://bugzilla.suse.com/
-  https://bugzilla.redhat.com/
-  https://www.openwall.com/lists/oss-security/
-  https://www.zerodayinitiative.com
-  https://lore.kernel.org/stable/
-  https://bugzilla.kernel.org/

``TODO: Define frequency of CIP Kernel CVE reports sharing with CIP users. Currently it's bi-weekly``

4.1 Security Analysis of CIP Kernel CVE 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

CIP Kernel maintainers rely on mainline kernel maintainers, Debian
Security team as well as other CVE database projects for detailed
security analysis for relevant CVEs. As CIP kernel only accepts patches
which are already reviewed/merged by Linus Torvalds hence at any point
of time CIP kernel does not maintain any local changes.

CIP kernel users are recommended to do a risk assessment of the CIP
kernel installed based on the specific uses cases where CVEs are open
and are having high severity.

The CVE report shared by CIP for CIP Kernel is for CIP kernel user’s
reference. Based on the CVE report as well as DSA published by Debian
Security Team CIP users are advised to update their systems to receive
latest security updates.

5. References 
-------------

1. CIP Core CVE handling

   https://gitlab.com/cip-project/cip-core/debian-cve-checker

2. CIP Kernel CVE handling

   https://gitlab.com/cip-project/cip-kernel/cip-kernel-sec

3. Debian Security FAQ Page

   https://www.debian.org/security/faq

4. Debian Security Bug Tracker.

   https://security-tracker.debian.org/tracker/
