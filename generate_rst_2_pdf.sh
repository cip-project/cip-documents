#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

#Path to logo
LOGO_PATH="../resources/CIP_Logo.png"

#Footer text
FOOTER_TEXT="© 2024 Civil Infrastructure Platform is a Linux Foundation Project.All Rights Reserved."

# Specify the output folder for PDFs
rstPDF="$SCRIPT_DIR/rstPDF"

# Create the output folder if it doesn't exist
if ! [ -d $rstPDF ]; then
	mkdir -p "$rstPDF"
fi

find "$SCRIPT_DIR" -type f -name "*.rst" -not -name "index.rst" | while read -r rst_file; do
    # Get the base name of the Markdown file (without extension)
    base_name=$(basename -- "$rst_file")
    dir_name=$(dirname "$rst_file")
    base_name_no_ext="${base_name%.rst}"
    dir_basename=$(basename "$dir_name")

    # Define the output paths for PDF files
    output_pdf="$rstPDF/$dir_basename"_"$base_name_no_ext.pdf"

    # Add the logo at the top of the .rst file
    echo ".. image:: $SCRIPT_DIR/resources/CIP_Logo.png
            :width: 300px
            :align: left
            :height: 300px
            :alt: CIP_logo" > $rst_file.tmp

    # Include a blank line after indented block i.e header block
    echo "" >> $rst_file.tmp

    cat $rst_file >> $rst_file.tmp && rm $rst_file
    echo >> $rst_file.tmp  # Blank line
    echo ".. header::

        ###Page###" >> $rst_file.tmp

    echo ".. footer:: $FOOTER_TEXT" >> $rst_file.tmp

    pushd "$dir_name" > /dev/null

    # Convert the RST files to PDF using rst2pdf
    if rst2pdf -q $rst_file.tmp  $output_pdf -e plantuml; then

        echo "Converted: $rst_file ==> $output_pdf"
    else
        echo "Failed to convert $rst_file"
    fi

    popd > /dev/null
done
