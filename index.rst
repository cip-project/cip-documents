.. CIP_documentation documentation master file, created by
   sphinx-quickstart on Thu Nov 16 14:37:21 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

CIP DOCUMENTS
==============

.. toctree::
    :maxdepth: 2
    :caption: CIP documentation

    README.rst
    glossary.rst

.. toctree::
    :maxdepth: 2
    :caption: process

    process/index.rst

.. toctree::
   :maxdepth: 2
   :caption: security

   security/index.rst

.. toctree::
   :maxdepth: 2
   :caption: testing

   testing/index.rst

.. toctree::
   :maxdepth: 2
   :caption: user

   user/index.rst

.. toctree::
    :maxdepth: 1
    :caption: TODO

    TODO.rst
